package vn.smap.dbprototype.dao.pojos;

import java.io.Serializable;

public class Column implements Serializable{
	public String name;
	public Object value;
	public int index;
	
	
	
	public Column( String name, int index ) {
		this.name = name;
		this.index = index;
	}
	
	public Column( String name, Object value, int index ) {
		this.name = name;
		this.value = value;
		this.index = index;
	}
	
	@Override
	public String toString() {
		String result = "" + index + ", " + name + ", " + value;
		return result;
	}
	
	public String getName() {
		if( name == null ) return "";
		return name;
	}
	
	public Object getValue() {
		if( value == null ) return "";
		return value;
	}
	
	public Object getIndex() {
		return index;
	}
}
