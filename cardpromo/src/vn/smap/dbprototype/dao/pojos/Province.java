package vn.smap.dbprototype.dao.pojos;

import java.io.Serializable;
import java.util.LinkedList;

public class Province implements Serializable {
	// private int mProvinceID;
	// private String mProvinceName;
	// public Province(int provinceID, String provinceName){
	// setmProvinceID(provinceID);
	// setmProvinceName(provinceName);
	// }
	//
	// @Override
	// public String toString(){
	// return mProvinceName;
	// }
	// public int getmProvinceID() {
	// return mProvinceID;
	// }
	// public void setmProvinceID(int mProvinceID) {
	// this.mProvinceID = mProvinceID;
	// }
	// public String getmProvinceName() {
	// return mProvinceName;
	// }
	// public void setmProvinceName(String mProvinceName) {
	// this.mProvinceName = mProvinceName;
	// }

	public static final String TableName = "province";

	public final Column clID = new Column("_id", 0);
	public final Column clName = new Column("Name", 1);

	public LinkedList<Column> ColumnsList = new LinkedList<Column>();

	public Province() {
		ColumnsList.add(clID);
		ColumnsList.add(clName);
	}

	public int getID() {
		int id = 0;
		try {
			id = (Integer) clID.value;
		} catch (Exception e) {
		}
		return id;
	}

	public String getName() {
		if (clName.value == null)
			return "";
		return clName.value.toString();
	}

	public void setID(int ID) {
		this.clID.value = new Integer(ID);
	}

	public void setName(String name) {
		if (name == null)
			name = "";
		clName.value = name;
	}
	
	public String toString(){
		return getName();
	}

}
