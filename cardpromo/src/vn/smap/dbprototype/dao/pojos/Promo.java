package vn.smap.dbprototype.dao.pojos;

import java.io.Serializable;
import java.util.LinkedList;

public class Promo implements Serializable{
	public static final String TableName = "promos";
	public static final String DBName = "PromosDB";
	
	public Column clID = new Column( "_id", 0 );
	public Column clPlaceID = new Column( "PlaceID", 1 );
	public Column clCardID = new Column( "CardID", 2 );
	public Column clDiscount = new Column( "Discount", 3 );
	public Column clShortConstrain = new Column( "ShortConstrain", 4 );
	public Column clConstrain = new Column( "Constrain", 5 );
	public Column clExpireDate = new Column( "ExpireDate", 6 );
	
	public LinkedList<Column> ColumnsList = new LinkedList<Column>();
	
	public Promo() {
		ColumnsList.add( clID );
		ColumnsList.add( clPlaceID );
		ColumnsList.add( clCardID );
		ColumnsList.add( clDiscount );
		ColumnsList.add( clShortConstrain );
		ColumnsList.add( clConstrain );
		ColumnsList.add( clExpireDate );		
	}
	
	public void setID( int id ) {
		this.clID.value = new Integer( id );
	}
	
	public void setPlaceID( int placeID ) {
		this.clPlaceID.value = new Integer( placeID );
	}
	
	public void setCardID( int cardID ) {
		this.clCardID.value = new Integer( cardID );
	}
	
	public void setDiscount( float discount ) {
		this.clDiscount.value = new Float( discount );
	}
	
	public void setShortConstrain( String shorConstrain ) {
		this.clShortConstrain.value = shorConstrain;
	}
	
	public void setConstrain( String constrain ) {
		this.clConstrain.value = constrain;
	}
	
	public void setExpireDate( String expireDate ) {
		this.clExpireDate.value = expireDate;
	}
	
	public int getID() {
		if( clID == null ) return 0;
		return (Integer) clID.value;
	}
	
	public int getPlaceID() {
		if( this.clPlaceID == null 
				|| this.clPlaceID.value == null ) return 0;
		return (Integer) this.clPlaceID.value;
	}
	
	public Integer getCardID() {
		if( this.clCardID == null 
				|| this.clCardID.value == null ) return 0;
		return (Integer) this.clPlaceID.value;
	}
	
	public Float getDiscount() {
		if( this.clDiscount == null 
				|| this.clDiscount.value == null ) return (float) 0;
		return (Float) this.clDiscount.value;
	}
	
	public String getShortConstrain() {
		if( this.clShortConstrain == null ) return "";
		return this.clShortConstrain.toString();
	}
	
	public String getConstrain() {
		if( this.clConstrain == null ) return "";
		return this.clConstrain.value.toString();
	} 
	
	public String getExpireDate() {
		if( this.clExpireDate == null ) return "";
		return this.clExpireDate.value.toString();
	}
	
	
	
}
