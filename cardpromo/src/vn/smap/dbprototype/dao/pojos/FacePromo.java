package vn.smap.dbprototype.dao.pojos;

import java.io.Serializable;
import java.util.LinkedList;

public class FacePromo implements Serializable {

	public static final String TableName = "face_promos";
	public static final String DBName = "FacePromosDB";

	public Column clID = new Column("_id", 0);
	public Column clShortCardName = new Column("ShortCardName", 1);
	public Column clBankName = new Column("BankName", 2);
	public Column clPlaceID = new Column("PlaceID", 3);
	public Column clCardID = new Column("CardID", 4);
	public Column clPlaceName = new Column("PlaceName", 5);
	public Column clPlaceImg = new Column("PlaceImg", 6);
	public Column clPlacePhone = new Column("PlacePhone", 7);
	public Column clPlaceAddress = new Column("PlaceAddress", 8);
	public Column clPlaceEmail = new Column("PlaceEmail", 9);
	public Column clStartDate = new Column("StartDate", 10);
	public Column clExpireDate = new Column("ExpireDate", 11);
	public Column clDiscount = new Column("Discount", 12);
	public Column clShortConstrain = new Column("ShortConstrain", 13);
	public Column clConstrain = new Column("Constrain", 14);
	public Column clCardName = new Column("CardName", 15);
	public Column clPlaceTypeID = new Column("PlaceTypeID", 16);
	public Column clBankID = new Column("BankID", 17);
	public Column clPlaceTypeName = new Column("PlaceTypeName", 18);
	public Column clProvinceID = new Column("ProvinceID", 19);
	public Column clPromosID = new Column("PromosID", 23);
	public Column clConcatCards = new Column("ConcatCards", 24);

	public LinkedList<Column> ColumnsList = new LinkedList<Column>();

	public FacePromo() {
		ColumnsList.add(clID);
		ColumnsList.add(clShortCardName);
		ColumnsList.add(clBankName);
		ColumnsList.add(clPlaceID);
		ColumnsList.add(clCardID);
		ColumnsList.add(clPlaceName);
		ColumnsList.add(clPlaceImg);
		ColumnsList.add(clPlacePhone);
		ColumnsList.add(clPlaceAddress);
		ColumnsList.add(clPlaceEmail);
		ColumnsList.add(clStartDate);
		ColumnsList.add(clExpireDate);
		ColumnsList.add(clDiscount);
		ColumnsList.add(clShortConstrain);
		ColumnsList.add(clConstrain);
		ColumnsList.add(clCardName);
		ColumnsList.add(clPlaceTypeID);
		ColumnsList.add(clBankID);
		ColumnsList.add(clPlaceTypeName);
		ColumnsList.add(clProvinceID);
		ColumnsList.add(clPromosID);
		ColumnsList.add(clConcatCards);
	}

	public void setID(int id) {
		this.clID.value = new Integer(id);
	}

	public void setShortCardName(String shortCardName) {
		if (shortCardName == null) {
			shortCardName = "";
		}
		this.clShortCardName.value = shortCardName;
	}
	
	public void setBankName(String bankName) {
		if (bankName == null) {
			bankName = "";
		}
		this.clBankName.value = bankName;
	}

	public void setPlaceID(int placeID) {
		this.clPlaceID.value = new Integer(placeID);
	}

	public void setCardID(int cardID) {
		this.clCardID.value = new Integer(cardID);
	}

	public void setPlaceName(String placeName) {
		if (placeName == null)
			placeName = "";
		this.clPlaceName.value = placeName;
	}

	public void setPlaceImg(String placeImg) {
		if (placeImg == null)
			placeImg = "";
		this.clPlaceImg.value = placeImg;
	}

	public void setPlacePhone(String placePhone) {
		if (placePhone == null)
			placePhone = "";
		this.clPlacePhone.value = placePhone;
	}

	public void setPlaceAddress(String placeAddress) {
		if (placeAddress == null)
			placeAddress = "";
		this.clPlaceAddress.value = placeAddress;
	}

	public void setPlaceEmail(String placeEmail) {
		if (placeEmail == null)
			placeEmail = "";
		this.clPlaceEmail.value = placeEmail;
	}

	public void setStartDate(String startDate) {
		if (startDate == null)
			startDate = "";
		this.clStartDate.value = startDate;
	}

	public void setExpireDate(String expireDate) {
		if (expireDate == null)
			expireDate = "";
		this.clExpireDate.value = expireDate;
	}

	public void setDiscount(float discount) {
		this.clDiscount.value = new Float(discount);
	}

	public void setShortConstrain(String shortConstrain) {
		if (shortConstrain == null)
			shortConstrain = "";
		this.clShortConstrain.value = shortConstrain;
	}

	public void setConstrain(String constrain) {
		if (constrain == null)
			constrain = "";
		this.clConstrain.value = constrain;
	}

	public void setCardName(String cardName) {
		if (cardName == null)
			cardName = "";
		this.clCardName.value = cardName;
	}

	public void setPlaceTypeID(int placeTypeID) {
		this.clPlaceTypeID.value = new Integer(placeTypeID);
	}

	public void setBankID(int bankID) {
		this.clBankID.value = new Integer(bankID);
	}

	public void setPlaceTypeName(String placeTypeName) {
		this.clPlaceTypeName.value = placeTypeName;
	}

	public void setProvinceID(int provinceID) {
		this.clProvinceID.value = new Integer(provinceID);
	}
	
	public void setPromosID(int promosID) {
		this.clPromosID.value = new Integer(promosID);
	}
	
	public void setConcatCards(String concatCards){
		this.clConcatCards.value = concatCards;
	}

	public int getID() {
		if (this.clID == null)
			return 0;
		return (Integer) clID.value;
	}
	
	public String getShortCardName(){
		if(this.clShortCardName == null){
			return "";
		}
		return this.clShortCardName.value.toString();
	}
	
	public String getBankName(){
		if(this.clBankName == null){
			return "";
		}
		return this.clBankName.value.toString();
	}

	public int getPlaceID() {
		if (this.clPlaceID == null)
			return 0;
		return (Integer) clPlaceID.value;
	}

	public int getCardID() {
		if (this.clCardID == null)
			return 0;
		return (Integer) clCardID.value;
	}

	public String getPlaceName() {
		if (this.clPlaceName == null)
			return "";
		return this.clPlaceName.value.toString();
	}

	public String getPlaceImg() {
		if (this.clPlaceImg == null)
			return "";
		return this.clPlaceImg.value.toString();
	}

	public String getPlacePhone() {
		if (this.clPlacePhone == null)
			return "";
		return this.clPlacePhone.value.toString();
	}

	public String getPlaceAddress() {
		if (this.clPlaceAddress == null)
			return "";
		return this.clPlaceAddress.value.toString();
	}

	public String getPlaceEmail() {
		if (this.clPlaceEmail == null)
			return "";
		return this.clPlaceEmail.value.toString();
	}

	public String getStartDate() {
		if (this.clStartDate == null)
			return "";
		return this.clStartDate.value.toString();
	}

	public String getExpireDate() {
		if (this.clExpireDate == null)
			return "";
		return this.clExpireDate.value.toString();
	}

	public float getDiscount() {
		if (this.clDiscount == null)
			return 0;
		return (Float) this.clDiscount.value;
	}

	public String getShortConstrain() {
		if (this.clShortConstrain == null)
			return "";
		return this.clShortConstrain.value.toString();
	}

	public String getConstrain() {
		if (this.clConstrain == null || this.clConstrain.value == null)
			return "Constrain value is NULL";
		return this.clConstrain.value.toString();
	}

	public String getCardName() {
		if (this.clCardName == null)
			return "";
		return this.clCardName.value.toString();
	}

	public int getPlaceTypeID() {
		if (this.clPlaceTypeID == null)
			return 0;
		return (Integer) this.clPlaceTypeID.value;
	}

	public int getBankID() {
		if (this.clBankID == null)
			return 0;
		return (Integer) this.clBankID.value;
	}

	public String getPlaceTypeName() {
		if (this.clPlaceTypeName == null)
			return "";
		return this.clPlaceTypeName.value.toString();
	}

	public int getProvinceID() {
		if (this.clProvinceID == null)
			return 0;
		return (Integer) this.clProvinceID.value;
	}
	
	public int getPromosID() {
		if (this.clPromosID == null)
			return 0;
		return (Integer) this.clPromosID.value;
	}
	
	public String getConcatCards(){
		if(this.clConcatCards == null){
			return "";
		}else{
			return this.clConcatCards.value.toString();
		}
	}

}
