package vn.smap.dbprototype.dao.pojos;

import java.io.Serializable;
import java.util.LinkedList;

public class Place implements Serializable{
	
	public static final String TableName = "places";
	public static final String DBName = "PlacesDB";
	
	public Column clID = new Column( "_id", 0 );
	public Column clName = new Column( "Name", 1 );
	public Column clTypeID = new Column( "TypeID", 2 );
	public Column clTypeName = new Column( "TypeName", 3 );
	public Column clAddress = new Column( "Address", 4 );
	public Column clPhone1 = new Column( "Phone1", 5 );
	public Column clPhone2 = new Column( "Phone2", 6 );
	public Column clEmail = new Column( "Email", 7 );
	public Column clImgPath = new Column( "ImgPath", 8 );
	
	public LinkedList<Column> ColumnsList = new LinkedList<Column>();
	
	public Place() {
		ColumnsList.add( clID );
		ColumnsList.add( clName );
		ColumnsList.add( clTypeID );
		ColumnsList.add( clTypeName );
		ColumnsList.add( clAddress );
		ColumnsList.add( clPhone1 );
		ColumnsList.add( clPhone2 );
		ColumnsList.add( clEmail );
		ColumnsList.add( clImgPath );
	}
	
	public void setID( int id ) {
		this.clID.value = new Integer( id );
	}
	
	public void setName( String placeName ) {
		if( placeName == null ) {
			this.clName.value = "";
			return;
		}
		this.clName.value = placeName;
	}
	
	public void setTypeID( int placeTypeID ) {
		this.clTypeID.value = new Integer( placeTypeID );
	}
	
	public void setTypeName( String placeTypeName ) {
		if( placeTypeName == null ) {
			this.clTypeName.value = "";
			return;
		}
		this.clTypeName.value = placeTypeName;
	}
	
	public void setAddress( String address ) {
		if( address == null ) address = "";
		this.clAddress.value = address;
	}
	
	public void setPhone1( String phone1 ) {
		if( phone1 == null ) phone1 = "";
		this.clPhone1.value = phone1;
	}
	
	public void setPhone2( String phone2 ) {
		if( phone2 == null ) phone2 = "";
		this.clPhone2.value = phone2;
	}
	
	public void setEmail( String email ) {
		if( email == null ) email = "";
		this.clEmail.value = email;
	}
	
	public void setImgPath( String imgPath ) {
		if( imgPath == null ) imgPath = "";
		this.clImgPath.value = imgPath;
	}
	
	public int getID() {
		if( this.clID == null ) return 0;
		return (Integer) this.clID.value;
	}
	
	public String getName() {
		if( this.clName == null ) return "";
		return this.clName.toString();
	}
	
	public int getTypeID() {
		if( this.clTypeID == null ) return 0;
		return (Integer) this.clTypeID.value;
	}
	
	public String getTypeName() {
		if( this.clTypeName == null ) return "";
		return this.clTypeName.value.toString();
	}
	
	public String getAddress() {
		if( this.clAddress == null ) return "";
		return this.clAddress.toString();
	}
	
	public String getPhone1() {
		if( this.clPhone1 == null ) return "";
		return this.clPhone1.toString();
	}
	
	public String getPhone2() {
		if( this.clPhone2 == null ) return "";
		return this.clPhone2.toString();
	}
	
	public String getEmail() {
		if( this.clEmail == null ) return "";
		return this.clEmail.toString();
	}
	
	public String getImgPath() {
		if( this.clImgPath == null ) return "";
		return this.clImgPath.toString();
	}
	
}
