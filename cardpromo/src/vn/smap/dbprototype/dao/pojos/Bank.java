package vn.smap.dbprototype.dao.pojos;

import java.io.Serializable;
import java.util.LinkedList;

public class Bank implements Serializable {

	public static final String TableName = "banks";
	public static final String DBName = "BanksDB";

	public final Column clID = new Column("_id", 0);
	public final Column clName = new Column("Name", 1);
	public final Column clShortName = new Column("ShortName", 2);
	public final Column clAddress = new Column("Address", 3);
	public final Column clRank = new Column("Rank", 10);
	public final Column clLogo = new Column("Logo", 14);

	public LinkedList<Column> ColumnsList = new LinkedList<Column>();

	public Bank() {
		ColumnsList.add(clID);
		ColumnsList.add(clName);
		ColumnsList.add(clShortName);
		ColumnsList.add(clAddress);
		ColumnsList.add(clRank);
		ColumnsList.add(clLogo);
	}

	public int getID() {
		int id = 0;
		try {
			id = (Integer) clID.value;
		} catch (Exception e) {
		}
		return id;
	}

	public String getName() {
		if (clName.value == null)
			return "";
		return clName.value.toString();
	}

	public String getShortName() {
		if (clShortName.value == null)
			return "";
		return clShortName.value.toString();
	}

	public String getAddress() {
		if (clAddress.value == null)
			return "";
		return clAddress.value.toString();
	}

	public int getRank() {
		int rank = 1;
		try {
			rank = (Integer) clRank.value;
		} catch (Exception e) {

		}
		return rank;
	}

	public String getLogo() {
		if (clLogo.value == null) {
			return "";
		}
		return clLogo.value.toString();
	}

	public void setID(int ID) {
		this.clID.value = new Integer(ID);
	}

	public void setName(String name) {
		if (name == null)
			name = "";
		clName.value = name;
	}

	public void setShortName(String shortName) {
		if (shortName == null)
			shortName = "";
		clShortName.value = shortName;
	}

	public void setAddress(String address) {
		if (address == null)
			address = "";
		clAddress.value = address;
	}
	
	public void setRank(int rank){
		this.clRank.value = new Integer(rank);
	}

	public void setLogo(String logo) {
		if (logo == null) {
			logo = "";
		}
		clLogo.value = logo;
	}
}
