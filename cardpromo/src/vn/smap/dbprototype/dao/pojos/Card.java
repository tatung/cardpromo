package vn.smap.dbprototype.dao.pojos;

import java.io.Serializable;
import java.util.LinkedList;

public class Card implements Serializable {

	public static final String TableName = "cards";
	public static final String DBName = "CardsDB";

	public Column clID = new Column("_id", 0);
	public Column clName = new Column("Name", 1);
	public Column clShortName = new Column("ShortName", 2);
	public Column clBankID = new Column("BankID", 3);
	public Column clImgPath = new Column("ImgPath", 4);
	public Column clBankName = new Column("BankName", 5);

	public LinkedList<Column> ColumnsList = new LinkedList<Column>();

	public Card() {
		ColumnsList.add(clID);
		ColumnsList.add(clName);
		ColumnsList.add(clShortName);
		ColumnsList.add(clBankID);
		ColumnsList.add(clImgPath);
		ColumnsList.add(clBankName);
	}

	@Override
	public String toString() {
		String result = "" + clID.value + ", name = " + clName.value
				+ ", BankID = " + clBankID.value + ", ImgPath = "
				+ clImgPath.value + ", BankName = " + clBankName.value;
		return result;
	}

	public int getID() {
		if (clID.value == null)
			return 0;
		return (Integer) clID.value;
	}

	public String getName() {
		if (clName.value == null)
			return "";
		return clName.value.toString();
	}

	public String getShortName() {
		if (clShortName.value == null)
			return "";
		return clShortName.value.toString();
	}

	public int getBankID() {
		if (clBankID.value == null)
			return 0;
		return (Integer) clBankID.value;
	}

	public String getBankName() {
		if (clBankName.value == null)
			return "";
		return clBankName.value.toString();
	}

	public String getImgPath() {
		if (clImgPath.value == null)
			return "";
		return clImgPath.value.toString();
	}

	public void setName(String name) {
		if (name == null)
			name = "";
		clName.value = name;
	}

	public void setShortName(String shortName) {
		if (shortName == null)
			shortName = "";
		clShortName.value = shortName;
	}

	public void setBankID(int bankid) {
		clBankID.value = (Integer) bankid;
	}

	public void setBankName(String bankName) {
		if (bankName == null)
			bankName = "";
		clBankName.value = bankName;
	}

	public void setImgPath(String imgPath) {
		if (imgPath == null)
			imgPath = "";
		clImgPath.value = imgPath;
	}

	public void setID(int id) {
		this.clID.value = new Integer(id);
	}

}
