package vn.smap.dbprototype.dao.pojos;

import java.io.Serializable;
import java.util.LinkedList;

public class PlaceType implements Serializable {

	public static String TableName = "placetypes";
	public static String DBName = "PlaceTypesDB";

	public Column clID = new Column("_id", 0);
	public Column clName = new Column("Name", 1);
	public Column clSort = new Column("Sort", 2);

	public LinkedList<Column> ColumnsList = new LinkedList<Column>();

	public PlaceType() {
		ColumnsList.add(clID);
		ColumnsList.add(clName);
		ColumnsList.add(clSort);
	}

	public void setID(int id) {
		this.clID.value = new Integer(id);
	}

	public void setName(String placeName) {
		if (placeName == null)
			placeName = "";
		this.clName.value = placeName;
	}

	public String getName() {
		return this.clName.value.toString();
	}

	public int getID() {
		return (Integer) this.clID.value;
	}
}
