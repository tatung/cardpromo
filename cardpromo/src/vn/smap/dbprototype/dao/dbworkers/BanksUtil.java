package vn.smap.dbprototype.dao.dbworkers;

import java.util.ArrayList;
import java.util.LinkedList;

import vn.smap.dbprototype.config.SysParams;
import vn.smap.dbprototype.dao.pojos.Bank;
import vn.smap.dbprototype.dao.pojos.Column;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class BanksUtil{
	
	private Context context;
	private SQLiteDatabase mDb = null;
	
	public BanksUtil(SQLiteDatabase db){
		mDb = db;
	}
	
	public ArrayList<Bank> select( String whereClause ) {
		return select( null, whereClause, null, null );
	}
	
	public ArrayList<Bank> select( String whereClause, String orderClause ) {
		return select( null, whereClause, null, orderClause );
	}
	
	public ArrayList<Bank> select( String selection, String[] args ) {
		return select( null, selection, args, null );
	}
	
	public ArrayList<Bank> select( String[] columnNames, String selection, String[] args ) {
		return select( columnNames, selection, args, null );
	}
	
	public ArrayList<Bank> select( String[] columnNames, String selection, String[] args, String orderClause ) {
		ArrayList<Bank> alBanks = new ArrayList<Bank>();
		Cursor c = null;
		try{
			c = mDb.query( Bank.TableName, columnNames, selection, args, null, null, orderClause );
			for( int i = 0; c.moveToPosition( i ); i++ ) {
				Bank b = new Bank();
				int id = c.getInt( b.clID.index );
				String name = c.getString( b.clName.index );
				String shortName = c.getString(b.clShortName.index);
				String address = c.getString( b.clAddress.index );
				int rank = c.getInt(b.clRank.index);
				String logo = c.getString(b.clLogo.index);
				
				b.setID( id );
				b.setName( name );
				b.setShortName(shortName);
				b.setAddress( address );
				b.setRank(rank);
				b.setLogo(logo);
				
				alBanks.add( b );
			}
		}catch( Exception ex ) {
			if( SysParams.IsDebug ) {
				String message = "BanksUtil.select() -> Exception.";
				message += ex.getMessage();
				Toast.makeText( context, message, Toast.LENGTH_LONG ).show();
			}
		}
		
		return alBanks;
	}
	
	public void delete( String whereClause ) {
		delete( whereClause, null );
	}
	
	public void delete( String whereClause, String[] args ) {
		if( whereClause == null ) {
			if( SysParams.IsDebug ) {
				String message = "BanksUtil.delete() -> whereClause can NOT be null.";
				Toast.makeText( context, message, Toast.LENGTH_LONG ).show();
			}
			return;
		}
		
		try {
			mDb.delete( Bank.TableName, whereClause, args );
			
		}catch( Exception ex ) {
			if( SysParams.IsDebug ) {
				String message = "BanksUtil.delete() -> ";
				message += ex.getMessage();
				Toast.makeText( context, message, Toast.LENGTH_LONG ).show();
			}
		}
	}
	
	public void insert( Bank b ) {
		ContentValues cv = new ContentValues();
		
		LinkedList<Column> columnList = b.ColumnsList;
		int columnsNumber = columnList.size();
		Column currColumn;
		Object currValue;
		Class currClass;
		
		for( int i = 0; i < columnsNumber; i++ ) {
			try {
				currColumn = columnList.get( i );
				currValue = currColumn.value;
				currClass = currValue.getClass();
				
				if( SysParams.IsDebug ) {
					if( currClass.isInstance( new Integer( 0 ) ) ) {
						cv.put( currColumn.name, (Integer) currValue );
						
					}else if( currClass.isInstance( new Long( 0 ) ) ) {
						cv.put( currColumn.name, (Long) currValue );
						
					}else if( currClass.isInstance( new Float( "0.1" ) ) ) {
						cv.put( currColumn.name, (Float) currValue );
						
					}else if( currClass.isInstance( new Double( "0.1" ) ) ) {
						cv.put( currColumn.name, (Double) currValue );
						
					}else if( currClass.isInstance( "fuck" ) ) {
						cv.put( currColumn.name, (String) currValue );
						
					}else if( currClass.isInstance( new Boolean( true ) ) ) {
						cv.put( currColumn.name, (Boolean) currValue );
					}
				}
			}catch( Exception ex ) {
				//do nothing.
			}
		}
		
		try {
			mDb.insert( Bank.TableName, null, cv );
			
		}catch( Exception ex ) {
			if( SysParams.IsDebug ) {
				String message = "BanksUtil.insert() -> Exception.";
				message += "\n" +  ex.getMessage();
				Toast.makeText( context, message, Toast.LENGTH_LONG ).show();
			}
		}
	}
}
