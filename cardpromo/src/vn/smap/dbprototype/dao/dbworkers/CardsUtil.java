package vn.smap.dbprototype.dao.dbworkers;

import java.util.ArrayList;
import java.util.LinkedList;

import vn.smap.dbprototype.config.SysParams;
import vn.smap.dbprototype.dao.pojos.Card;
import vn.smap.dbprototype.dao.pojos.Column;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class CardsUtil {

	private Context context;
	Card card = new Card();
	private SQLiteDatabase db = null;

	String DropTable = "drop table if exist " + Card.TableName;

	public CardsUtil(SQLiteDatabase db) {
		this.db = db;
	}

	public ArrayList<Card> select(String whereClause) {
		return select(null, whereClause, null, null);
	}

	public ArrayList<Card> select(String whereClause, String order) {
		return select(null, whereClause, null, order);
	}

	public ArrayList<Card> select(String selection, String[] args) {
		return select(null, selection, args, null);
	}

	public ArrayList<Card> select(String[] columns, String whereClause) {
		return select(columns, whereClause, null, null);
	}

	public ArrayList<Card> select(String[] columns, String selection,
			String[] args, String order) {
		// SQLiteDatabase db = new CardsUtil( context ).getReadableDatabase();
		ArrayList<Card> alCards = new ArrayList<Card>();
		Cursor c = null;
		try {
			c = db.query(Card.TableName, columns, selection, args, null, null,
					order);
			for (int i = 0; c.moveToPosition(i); i++) {
				Card card = new Card();
				int id = c.getInt(card.clID.index);
				String name = c.getString(card.clName.index);
				String shortName = c.getString(card.clShortName.index);
				String imgPath = c.getString(card.clImgPath.index);
				int bankID = c.getInt(card.clBankID.index);
				String bankName = c.getString(card.clBankName.index);

				card.setID(id);
				card.setName(name);
				card.setShortName(shortName);
				card.setImgPath(imgPath);
				card.setBankID(bankID);
				card.setBankName(bankName);
				alCards.add(card);
			}
		} catch (Exception ex) {
			if (SysParams.IsDebug) {
				String message = "CardsUtil.select() -> ";
				message += ex.getMessage();
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}
		}
		// db.close();
		return alCards;
	}

	public void delete(String whereClause) {
		delete(whereClause, null);
	}

	public void delete(String whereClause, String[] args) {
		// SQLiteDatabase db = new CardsUtil( context ).getWritableDatabase();
		try {
			db.delete(Card.TableName, whereClause, args);
		} catch (Exception ex) {
			if (SysParams.IsDebug) {
				String message = "CardsUtil.delete() -> ";
				message += ex.getMessage();
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}
		}
		// db.close();
	}

	public void insert(Card c) {
		// SQLiteDatabase db = new CardsUtil( context ).getWritableDatabase();

		ContentValues cv = new ContentValues();
		LinkedList<Column> cl = c.ColumnsList;
		int size = cl.size();
		Column column;
		Object value;
		Class vl;

		for (int i = 0; i < size; i++) {
			try {
				column = cl.get(i);

				value = column.value;
				vl = value.getClass();

				if (vl.isInstance(new Integer(0))) {
					cv.put(column.name, (Integer) value);

				} else if (vl.isInstance(new Long("0"))) {
					cv.put(column.name, (Long) value);

				} else if (vl.isInstance(new Float("0.1"))) {
					cv.put(column.name, (Float) value);

				} else if (vl.isInstance(new Double("0.1"))) {
					cv.put(column.name, (Double) value);

				} else if (vl.isInstance(new Boolean(true))) {
					cv.put(column.name, (Boolean) value);

				} else if (vl.isInstance("fuck")) {
					cv.put(column.name, String.valueOf(value));
				}

			} catch (Exception ex) {
				if (SysParams.IsDebug) {
					String msg = "CardsUtil.insert()1 -> ";
					msg += ex.getMessage();
					Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
				}
			}
		}

		try {
			db.insert(Card.TableName, null, cv);
		} catch (Exception ex) {
			if (SysParams.IsDebug) {
				String message = "CardsUtil.insert() -> ";
				message += ex.getMessage();
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}
		}
		// db.close();
	}

}
