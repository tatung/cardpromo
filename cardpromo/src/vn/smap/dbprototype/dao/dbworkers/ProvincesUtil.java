package vn.smap.dbprototype.dao.dbworkers;

import java.util.ArrayList;
import java.util.LinkedList;

import vn.smap.dbprototype.config.SysParams;
import vn.smap.dbprototype.dao.pojos.Column;
import vn.smap.dbprototype.dao.pojos.Province;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class ProvincesUtil {
	private Context context;
	private SQLiteDatabase mDb = null;
	
	public ProvincesUtil(SQLiteDatabase db){
		mDb = db;
	}
	
	public ArrayList<Province> select( String whereClause ) {
		return select( null, whereClause, null, null );
	}
	
	public ArrayList<Province> select( String whereClause, String orderClause ) {
		return select( null, whereClause, null, orderClause );
	}
	
	public ArrayList<Province> select( String selection, String[] args ) {
		return select( null, selection, args, null );
	}
	
	public ArrayList<Province> select( String[] columnNames, String selection, String[] args ) {
		return select( columnNames, selection, args, null );
	}
	
	public ArrayList<Province> select( String[] columnNames, String selection, String[] args, String orderClause ) {
		ArrayList<Province> alProvinces = new ArrayList<Province>();
		Cursor c = null;
		try{
			c = mDb.query( Province.TableName, columnNames, selection, args, null, null, orderClause );
			for( int i = 0; c.moveToPosition( i ); i++ ) {
				Province b = new Province();
				int id = c.getInt( b.clID.index );
				String name = c.getString( b.clName.index );
				
				b.setID( id );
				b.setName( name );
				alProvinces.add( b );
			}
		}catch( Exception ex ) {
			if( SysParams.IsDebug ) {
				String message = "ProvincesUtil.select() -> Exception.";
				message += ex.getMessage();
				Toast.makeText( context, message, Toast.LENGTH_LONG ).show();
			}
		}
		
		return alProvinces;
	}
	
	public void delete( String whereClause ) {
		delete( whereClause, null );
	}
	
	public void delete( String whereClause, String[] args ) {
		if( whereClause == null ) {
			if( SysParams.IsDebug ) {
				String message = "ProvincesUtil.delete() -> whereClause can NOT be null.";
				Toast.makeText( context, message, Toast.LENGTH_LONG ).show();
			}
			return;
		}
		
		try {
			mDb.delete( Province.TableName, whereClause, args );
			
		}catch( Exception ex ) {
			if( SysParams.IsDebug ) {
				String message = "ProvincesUtil.delete() -> ";
				message += ex.getMessage();
				Toast.makeText( context, message, Toast.LENGTH_LONG ).show();
			}
		}
	}
	
	public void insert( Province b ) {
		ContentValues cv = new ContentValues();
		
		LinkedList<Column> columnList = b.ColumnsList;
		int columnsNumber = columnList.size();
		Column currColumn;
		Object currValue;
		Class currClass;
		
		for( int i = 0; i < columnsNumber; i++ ) {
			try {
				currColumn = columnList.get( i );
				currValue = currColumn.value;
				currClass = currValue.getClass();
				
				if( SysParams.IsDebug ) {
					if( currClass.isInstance( new Integer( 0 ) ) ) {
						cv.put( currColumn.name, (Integer) currValue );
						
					}else if( currClass.isInstance( new Long( 0 ) ) ) {
						cv.put( currColumn.name, (Long) currValue );
						
					}else if( currClass.isInstance( new Float( "0.1" ) ) ) {
						cv.put( currColumn.name, (Float) currValue );
						
					}else if( currClass.isInstance( new Double( "0.1" ) ) ) {
						cv.put( currColumn.name, (Double) currValue );
						
					}else if( currClass.isInstance( "fuck" ) ) {
						cv.put( currColumn.name, (String) currValue );
						
					}else if( currClass.isInstance( new Boolean( true ) ) ) {
						cv.put( currColumn.name, (Boolean) currValue );
					}
				}
			}catch( Exception ex ) {
				//do nothing.
			}
		}
		
		try {
			mDb.insert( Province.TableName, null, cv );
			
		}catch( Exception ex ) {
			if( SysParams.IsDebug ) {
				String message = "ProvincesUtil.insert() -> Exception.";
				message += "\n" +  ex.getMessage();
				Toast.makeText( context, message, Toast.LENGTH_LONG ).show();
			}
		}
	}

}
