package vn.smap.dbprototype.dao.dbworkers;

import java.util.ArrayList;
import java.util.LinkedList;

import vn.smap.dbprototype.config.SysParams;
import vn.smap.dbprototype.dao.pojos.Column;
import vn.smap.dbprototype.dao.pojos.Place;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class PlacesUtil {

	private Context context;
	private Place p = new Place();
	private SQLiteDatabase db = null;
	private final String DropTable = "drop table if exists " + Place.TableName;
	
	public PlacesUtil(SQLiteDatabase db){
		this.db = db;
	}

	public ArrayList<Place> select( String whereClause ) {
		return select( null, whereClause, null, null ); 
	}
	
	public ArrayList<Place> select( String whereClause, String[] args ) {
		return select( null, whereClause, args, null ); 
	}
	
	public ArrayList<Place> select( String[] colums, String whereClause ) {
		return select( colums, whereClause, null, null );
	}
	
	public ArrayList<Place> select( String whereClause, String order ) {
		return select( null, whereClause, null, order );
	}
	
	public ArrayList<Place> select( String[] columns, String whereClause, String order ) {
		return select( columns, whereClause, null, order );
	}
	
	public ArrayList<Place> select( String[] columns, String selection, String[] args, String order ) {
//		SQLiteDatabase db = new PlacesUtil( context ).getReadableDatabase();
		ArrayList<Place> alPlaces = new ArrayList<Place>();
		Cursor c = null;
		try {
			c = db.query( Place.TableName, columns, selection, args, null, null, order );
			
			for( int i = 0; c.moveToPosition( i ); i++ ) {
				Place p = new Place();
				String address = c.getString( p.clAddress.index );
				String email = c.getString( p.clEmail.index );
				int id = c.getInt( p.clID.index );
				String imgPath = c.getString( p.clImgPath.index );
				String name = c.getString( p.clName.index );
				String phone1 = c.getString( p.clPhone1.index );
				String phone2 = c.getString( p.clPhone2.index );
				int typeID = c.getInt( p.clTypeID.index );
				String typeName = c.getString( p.clTypeName.index );
				
				p.setEmail( email );
				p.setAddress( address );
				p.setID( id );
				p.setImgPath( imgPath );
				p.setName( name );
				p.setPhone1( phone1 );
				p.setPhone2( phone2 );
				p.setTypeID( typeID );
				p.setTypeName( typeName );
				
				alPlaces.add( p );
			}
			
		}catch( Exception ex ) {
			String msg = "PlacesUtil.select() -> ";
			msg += ex.getMessage();
			Toast.makeText( context, msg, Toast.LENGTH_LONG ).show();
		}
//		db.close();
		return alPlaces;
	}

	public void insert( Place p ) {
//		SQLiteDatabase db = new PlacesUtil( context ).getWritableDatabase();
		ContentValues cv = new ContentValues();
		
		Column cl;
		Object value;
		Class c;
		LinkedList<Column> columnsList = p.ColumnsList;
		int size = columnsList.size();
		
		for( int i = 0; i < size; i++ ) {
			cl = columnsList.get( i );
			value = cl.value;
			
			if( value != null ) {
				c = value.getClass();
				if( c.isInstance( new Integer( 1 ) ) ) {
					cv.put( cl.name, (Integer) value );
					
				}else if( c.isInstance( new Long( 1) ) ) {
					cv.put( cl.name, (Long) value );
					
				}else if( c.isInstance( new Float( 0.1 ) ) ) {
					cv.put( cl.name, (Float) value );
					
				}else if( c.isInstance( new Double( 0.1 ) ) ) {
					cv.put( cl.name, (Double) value );
					
				}else if( c.isInstance( new Boolean( true ) ) ) {
					cv.put( cl.name, (Boolean) value );
					
				}else if( c.isInstance( "fuck" ) ) {
					cv.put( cl.name, value.toString() );
				}
				
			}
		}
		try {
			db.insert( Place.TableName, null, cv );
		}catch( Exception ex ) {
			if( SysParams.IsDebug ) {
				String msg = "PlacesUtil.insert() -> ";
				msg += ex.getMessage();
				Toast.makeText( context, msg, Toast.LENGTH_LONG ).show();
			}
		}
//		db.close();
	}
	
	public void delete( String whereClause ) {
		delete( whereClause, null );
	}
	
	public void delete( String whereClause, String[] args ) {
		if( whereClause == null ) {
			if( SysParams.IsDebug ) {
				String msg = "PlacesUtil.detele() -> ";
				msg += " whereClause can NOT be null";
				Toast.makeText( context, msg, Toast.LENGTH_LONG ).show();
			}
			return;
		}
		
//		SQLiteDatabase db = new PlacesUtil( context ).getWritableDatabase();
		try {
			db.delete( Place.TableName, whereClause, args );
		}catch( Exception ex ) {}
//		db.close();
	}

}
