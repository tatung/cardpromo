package vn.smap.dbprototype.dao.dbworkers;

import java.util.ArrayList;
import java.util.LinkedList;

import vn.smap.dbprototype.config.SysParams;
import vn.smap.dbprototype.dao.pojos.Column;
import vn.smap.dbprototype.dao.pojos.Promo;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class PromosUtil {

	private Context context;
	private SQLiteDatabase db = null;
	
	public PromosUtil(SQLiteDatabase db){
		this.db = db;
	}
	
	public ArrayList<Promo> select( String whereClause ) {
		return select( null, whereClause, null, null );
	}
	
	public ArrayList<Promo> select( String whereClause, String[] args ) {
		return select( null, whereClause, args, null );
	}
	
	public ArrayList<Promo> select( String whereClause, String[] args, String order ) {
		return select( null, whereClause, args, order );
	}
	
	public ArrayList<Promo> select( String[] columns, String whereClause ) {
		return select( columns, whereClause );
	}
	
	public ArrayList<Promo> select( String[] columns, String whereClause, String order ) {
		return select( columns, whereClause, null, order );
	}
	
	public ArrayList<Promo> select( String[] columns, String selection, String[] args, String order ) {
//		SQLiteDatabase db = new PromosUtil( context ).getReadableDatabase();
		ArrayList<Promo> alPromos = new ArrayList<Promo>();
		Cursor c = null;
		try {
			c = db.query( Promo.TableName, columns, selection, args, null, null, order );
			
			for( int i = 0; c.moveToPosition( i ); i++ ) {
				Promo p = new Promo();
				
				int cardID = c.getInt( p.clCardID.index );
				String constrain = c.getString( p.clConstrain.index );
				float discount = c.getFloat( p.clDiscount.index );
				String expireDate = c.getString( p.clExpireDate.index );
				int id = c.getInt( p.clID.index );
				int placeID = c.getInt( p.clPlaceID.index );
				String shortConstrain = c.getString( p.clShortConstrain.index );
				
				p.setCardID( cardID );
				p.setConstrain( constrain );
				p.setDiscount( discount );
				p.setExpireDate( expireDate );
				p.setID( id );
				p.setPlaceID( placeID );
				p.setShortConstrain( shortConstrain );
				
				alPromos.add( p );
			}
		}catch( Exception ex ) {
			if( SysParams.IsDebug ) {
				String msg = "PromosUtil.select() -> ";
				msg += ex.getMessage();
				Toast.makeText( context, msg, Toast.LENGTH_LONG ).show();
			}
		}
//		db.close();
		return alPromos;
	}

	public void insert( Promo p ) {
//		SQLiteDatabase db = new PromosUtil( context ).getWritableDatabase();
		ContentValues cv = new ContentValues();
		
		Column cl;
		Object value;
		Class c;
		LinkedList<Column> cll = p.ColumnsList;
		int size = cll.size();
		
		for( int i = 0; i < size; i++ ) {
			cl = cll.get( i );
			value = cl.value;
			if( value != null ) {
				
				c = value.getClass();
				if( c.isInstance( new Integer( 1 ) ) ) {
					cv.put( cl.name, (Integer) value );
					
				}else if( c.isInstance( new Long( 1 )  ) ) {
					cv.put( cl.name, (Long) value );
					
				}else if( c.isInstance( new Float( 1.1 ) ) ) {
					cv.put( cl.name, (Float) value );
					
				}else if( c.isInstance( new Double( 1.2 ) ) ) {
					cv.put( cl.name, (Double) value );
					
				}else if( c.isInstance( new Boolean( true ) ) ) {
					cv.put( cl.name, (Boolean) value );
					
				}else if( c.isInstance( "fuck" ) ) {
					cv.put( cl.name, value.toString() );
				}
			}
		}
		try {
			db.insert( Promo.TableName, null, cv );
		}catch( Exception ex ) {
			if( SysParams.IsDebug ) {
				String msg = "PromosUtil.insert() -> ";
				msg += ex.getMessage();
				Toast.makeText( context, msg, Toast.LENGTH_LONG ).show();
			}
		}
//		db.close();
	}
	
	public void delete( String whereClause ) {
		delete( whereClause, null );
	}
	
	public void delete( String whereClause, String[] args ) {
//		SQLiteDatabase db = new PromosUtil( context ).getWritableDatabase();
		try {
			db.delete( Promo.TableName, whereClause, args );
		}catch( Exception ex ) {
			if( SysParams.IsDebug ) {
				String msg = "PromosUtil.delete() -> ";
				msg += ex.getMessage();
				Toast.makeText( context, msg, Toast.LENGTH_LONG ).show();
			}
		}
//		db.close();
	}
}
