package vn.smap.dbprototype.dao.dbworkers;

import java.util.ArrayList;
import java.util.LinkedList;

import vn.smap.dbprototype.config.SysParams;
import vn.smap.dbprototype.dao.pojos.Column;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class FacePromosUtil {

	private Context context;
	private SQLiteDatabase db = null;

	public FacePromosUtil(SQLiteDatabase db) {
		this.db = db;
	}

	public ArrayList<FacePromo> select(String whereClause) {
		return select(null, whereClause, null, null, null, null);
	}

	public ArrayList<FacePromo> select(String whereClause, String[] args) {
		return select(null, whereClause, args, null, null, null);
	}

	public ArrayList<FacePromo> select(String whereClause, String order) {
		return select(null, whereClause, null, order, null, null);
	}

	public ArrayList<FacePromo> select(String[] columns, String whereClause) {
		return select(columns, whereClause, null, null, null, null);
	}

	public ArrayList<FacePromo> select(String[] columns, String whereClause,
			String order) {
		return select(columns, whereClause, null, order, null, null);
	}

	public ArrayList<FacePromo> select(String whereClause, String order,
			Short limitCount, Integer offset) {
		return select(null, whereClause, null, order, limitCount, offset);
	}

	public ArrayList<FacePromo> select(String[] columns, String selection,
			String[] args, String order, Short limitCount, Integer offset) {
		ArrayList<FacePromo> alFacePromos;
		Cursor c = null;
		try {
			String limitStatement = (String) (offset == null ? limitCount
					: (limitCount == null ? null : offset + "," + limitCount));
			c = db.query(FacePromo.TableName, columns, selection, args, null,
					null, order, limitStatement);
			alFacePromos = cursorToArrayList(columns, c);
			return alFacePromos;
		} catch (Exception ex) {
			String message = "FacePromosUtil.select() ->  ";
			message += ex.getMessage();
			Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			return new ArrayList<FacePromo>();
		}

	}

	public ArrayList<FacePromo> select(String whereClause, String groupBy,
			String order, Short limitCount, Integer offset) {
		return select(null, whereClause, null, groupBy, order, limitCount,
				offset);
	}

	public ArrayList<FacePromo> select(String[] columns, String selection,
			String[] args, String groupBy, String order, Short limitCount,
			Integer offset) {
		ArrayList<FacePromo> alFacePromos = new ArrayList<FacePromo>();
		FacePromo facePromo = new FacePromo();
		Cursor c = null;
		try {
			String limitStatement = (String) (offset == null ? limitCount
					: (limitCount == null ? null : offset + "," + limitCount));

			String rawQuery = "";
			rawQuery = "SELECT *, GROUP_CONCAT(DISTINCT(ShortCardName)) AS ConcatCards"
					+ " FROM " + FacePromo.TableName;
			rawQuery += " WHERE "
					+ selection
					+ " GROUP BY "
					+ groupBy
					+ " ORDER BY "
					+ order
					+ (limitStatement == null ? "" : " LIMIT " + limitStatement);

			c = db.rawQuery(rawQuery, null);

			for (int i = 0; c.moveToPosition(i); i++) {
				FacePromo fp = new FacePromo();

				String shortCardName = c.getString(fp.clShortCardName.index);
				String bankName = c.getString(fp.clBankName.index);
				int bankID = c.getInt(fp.clBankID.index);
				int cardID = c.getInt(fp.clCardID.index);
				String cardName = c.getString(fp.clCardName.index);
				float discount = c.getFloat(fp.clDiscount.index);
				String startDate = c.getString(fp.clStartDate.index);
				String expireDate = c.getString(fp.clExpireDate.index);
				int id = c.getInt(fp.clID.index);
				String placeName = c.getString(fp.clPlaceName.index);
				String placeAddress = c.getString(fp.clPlaceAddress.index);
				String placeEmail = c.getString(fp.clPlaceEmail.index);
				int placeID = c.getInt(fp.clPlaceID.index);
				String imgPath = c.getString(fp.clPlaceImg.index);
				String placePhone = c.getString(fp.clPlacePhone.index);
				int placeTypeID = c.getInt(fp.clPlaceTypeID.index);
				String shortConstrain = c.getString(fp.clShortConstrain.index);
				String constrain = c.getString(fp.clConstrain.index);
				String placeTypeName = c.getString(fp.clPlaceTypeName.index);
				int provinceID = c.getInt(fp.clProvinceID.index);

				String concatCards = c.getString(fp.clConcatCards.index);

				fp.setShortCardName(shortCardName);
				fp.setBankName(bankName);
				fp.setBankID(bankID);
				fp.setCardID(cardID);
				fp.setCardName(cardName);
				fp.setDiscount(discount);
				fp.setStartDate(startDate);
				fp.setExpireDate(expireDate);
				fp.setID(id);
				fp.setPlaceName(placeName);
				fp.setPlaceAddress(placeAddress);
				fp.setPlaceEmail(placeEmail);
				fp.setPlaceID(placeID);
				fp.setPlaceImg(imgPath);
				fp.setPlacePhone(placePhone);
				fp.setPlaceTypeID(placeTypeID);
				fp.setShortConstrain(shortConstrain);
				fp.setConstrain(constrain);
				fp.setPlaceTypeName(placeTypeName);
				fp.setProvinceID(provinceID);

				fp.setConcatCards(concatCards);

				alFacePromos.add(fp);
			}

			return alFacePromos;
		} catch (Exception ex) {
			String message = "FacePromosUtil.select() ->  ";
			message += ex.getMessage();
			Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			return new ArrayList<FacePromo>();
		}
	}

	public ArrayList<FacePromo> cursorToArrayList(String[] columns, Cursor c) {
		ArrayList<FacePromo> alFacePromos = new ArrayList<FacePromo>();
		if (columns != null) {
			throw new UnsupportedOperationException();
		} else {
			for (int i = 0; c.moveToPosition(i); i++) {
				FacePromo fp = new FacePromo();

				String shortCardName = c.getString(fp.clShortCardName.index);
				String bankName = c.getString(fp.clBankName.index);
				int bankID = c.getInt(fp.clBankID.index);
				int cardID = c.getInt(fp.clCardID.index);
				String cardName = c.getString(fp.clCardName.index);
				float discount = c.getFloat(fp.clDiscount.index);
				String startDate = c.getString(fp.clStartDate.index);
				String expireDate = c.getString(fp.clExpireDate.index);
				int id = c.getInt(fp.clID.index);
				String placeName = c.getString(fp.clPlaceName.index);
				String placeAddress = c.getString(fp.clPlaceAddress.index);
				String placeEmail = c.getString(fp.clPlaceEmail.index);
				int placeID = c.getInt(fp.clPlaceID.index);
				String imgPath = c.getString(fp.clPlaceImg.index);
				String placePhone = c.getString(fp.clPlacePhone.index);
				int placeTypeID = c.getInt(fp.clPlaceTypeID.index);
				String shortConstrain = c.getString(fp.clShortConstrain.index);
				String constrain = c.getString(fp.clConstrain.index);
				String placeTypeName = c.getString(fp.clPlaceTypeName.index);
				int provinceID = c.getInt(fp.clProvinceID.index);

				fp.setShortCardName(shortCardName);
				fp.setBankName(bankName);
				fp.setBankID(bankID);
				fp.setCardID(cardID);
				fp.setCardName(cardName);
				fp.setDiscount(discount);
				fp.setStartDate(startDate);
				fp.setExpireDate(expireDate);
				fp.setID(id);
				fp.setPlaceName(placeName);
				fp.setPlaceAddress(placeAddress);
				fp.setPlaceEmail(placeEmail);
				fp.setPlaceID(placeID);
				fp.setPlaceImg(imgPath);
				fp.setPlacePhone(placePhone);
				fp.setPlaceTypeID(placeTypeID);
				fp.setShortConstrain(shortConstrain);
				fp.setConstrain(constrain);
				fp.setPlaceTypeName(placeTypeName);
				fp.setProvinceID(provinceID);

				alFacePromos.add(fp);
			}
		}
		return alFacePromos;
	}

	public void insert(FacePromo fp) {
		// SQLiteDatabase db = new FacePromosUtil( context
		// ).getWritableDatabase();
		ContentValues cv = new ContentValues();

		try {
			LinkedList<Column> columnsList = fp.ColumnsList;
			int size = columnsList.size();
			Column cl;
			Object value;
			Class c;

			for (int i = 0; i < size; i++) {
				try {
					cl = columnsList.get(i);
					value = cl.value;
					if (value != null) {
						c = value.getClass();

						if (c.isInstance(new Integer(1))) {
							cv.put(cl.name, (Integer) value);

						} else if (c.isInstance(new Long(1))) {
							cv.put(cl.name, (Long) value);

						} else if (c.isInstance(new Float(1))) {
							cv.put(cl.name, (Float) value);

						} else if (c.isInstance(new Double(1))) {
							cv.put(cl.name, (Double) value);

						} else if (c.isInstance(new Boolean(true))) {
							cv.put(cl.name, (Boolean) value);

						} else if (c.isInstance(new String("fuck"))) {
							cv.put(cl.name, String.valueOf(value));
						}
					}
				} catch (Exception e) {
					if (SysParams.IsDebug) {
						String message = "FacePromosUtil.insert() (inside for loop)-> ";
						message += e.getMessage();
						// message += "\n" + CreateTable;
						Toast.makeText(context, message, Toast.LENGTH_LONG)
								.show();
					}
				}
			}
			db.insert(FacePromo.TableName, null, cv);

		} catch (Exception ex) {
			if (SysParams.IsDebug) {
				String message = "FacePromosUtil.insert() -> ";
				message += ex.getMessage();
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}
		}
		// db.close();
	}

	public void delete(String whereClause, String[] args) {
		if (whereClause == null) {
			if (SysParams.IsDebug) {
				String message = "FacePromosUtil.delete() -> ";
				message += " whereClause can NOT be null";
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}
			return;
		}
		// SQLiteDatabase db = new FacePromosUtil( context
		// ).getWritableDatabase();
		try {
			db.delete(FacePromo.TableName, whereClause, args);

		} catch (Exception ex) {
			if (SysParams.IsDebug) {
				String msg = "FacePromosUtil.delete() -> ";
				msg += ex.getMessage();
				Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
			}
		}
		// db.close();
	}
}
