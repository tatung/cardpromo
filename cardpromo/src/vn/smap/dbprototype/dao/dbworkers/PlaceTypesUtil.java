package vn.smap.dbprototype.dao.dbworkers;

import java.util.ArrayList;
import java.util.LinkedList;

import vn.smap.dbprototype.config.SysParams;
import vn.smap.dbprototype.dao.pojos.Column;
import vn.smap.dbprototype.dao.pojos.PlaceType;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class PlaceTypesUtil {

	private Context context;
	private SQLiteDatabase db = null;

	public PlaceTypesUtil(SQLiteDatabase db) {
		this.db = db;
	}

	public ArrayList<PlaceType> select(String whereClause) {
		return select(null, whereClause, null, null);
	}

	public ArrayList<PlaceType> selectOrder(String whereClause, String order) {
		return select(null, whereClause, null, order);
	}

	public ArrayList<PlaceType> select(String whereClause, String[] args) {
		return select(null, whereClause, args, null);
	}

	public ArrayList<PlaceType> select(String[] columns, String whereClause) {
		return select(columns, whereClause, null, null);
	}

	public ArrayList<PlaceType> select(String[] columns, String whereclause,
			String order) {
		return select(columns, whereclause, null, order);
	}

	public ArrayList<PlaceType> select(String[] columns, String selection,
			String[] args, String order) {
		// SQLiteDatabase db = new PlaceTypesUtil( context
		// ).getReadableDatabase();
		ArrayList<PlaceType> alPlaceType = new ArrayList<PlaceType>();
		Cursor c = null;
		try {
			c = db.query(PlaceType.TableName, columns, selection, args, null,
					null, order);

			for (int i = 0; c.moveToPosition(i); i++) {
				PlaceType pt = new PlaceType();
				int id = c.getInt(pt.clID.index);
				String name = c.getString(pt.clName.index);

				pt.setID(id);
				pt.setName(name);

				alPlaceType.add(pt);
			}
		} catch (Exception ex) {
			if (SysParams.IsDebug) {
				String msg = "PlaceTypesUtil.select() -> ";
				msg += ex.getMessage();
				Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
			}
		}
		// db.close();
		return alPlaceType;
	}

	public void insert(PlaceType pt) {
		// SQLiteDatabase db = new PlaceTypesUtil( context
		// ).getWritableDatabase();
		ContentValues cv = new ContentValues();

		Column cl;
		Object value;
		Class c;
		LinkedList<Column> columnsList = pt.ColumnsList;
		int size = columnsList.size();

		for (int i = 0; i < size; i++) {
			cl = columnsList.get(i);
			value = cl.value;
			if (value != null) {

				c = value.getClass();
				if (c.isInstance(new Integer(1))) {
					cv.put(cl.name, (Integer) value);

				} else if (c.isInstance(new Long(1))) {
					cv.put(cl.name, (Long) value);

				} else if (c.isInstance(new Float(1))) {
					cv.put(cl.name, (Float) value);

				} else if (c.isInstance(new Double(1))) {
					cv.put(cl.name, (Double) value);

				} else if (c.isInstance(new Boolean(true))) {
					cv.put(cl.name, (Boolean) value);

				} else if (c.isInstance("Fuck")) {
					cv.put(cl.name, value.toString());
				}
			}
		}

		try {
			db.insert(PlaceType.TableName, null, cv);
		} catch (Exception ex) {
			if (SysParams.IsDebug) {
				String msg = "PlaceTypesUtil.insert() -> ";
				msg += ex.getMessage();
				Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
			}
		}
		// db.close();
	}

	public void delete(String whereClause) {
		delete(whereClause, null);
	}

	public void delete(String whereClause, String[] args) {
		if (whereClause == null) {
			if (SysParams.IsDebug) {
				String msg = "PlaceTypesClause.delete() -> whereClause can NOT be null";
				Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
			}
			return;
		}
		// SQLiteDatabase db = new PlaceTypesUtil( context
		// ).getWritableDatabase();
		try {
			db.delete(PlaceType.TableName, whereClause, args);
		} catch (Exception e) {
			if (SysParams.IsDebug) {
				String msg = "PlaceTypesUtil.delete() -> ";
				msg += e.getMessage();
				Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
			}
		}
		// db.close();
	}
}
