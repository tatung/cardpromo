package vn.smap.cardpromo.utils;

import vn.smap.cardpromo.configs.AppConfigParams;

public final class StaticData {
	public static final class ExtraKeys{
		public static final String KEYWORDS=AppConfigParams.ROOT_NAMESPACE + ".KEYWORDS";
		public static final String DB_WRAPPER=AppConfigParams.ROOT_NAMESPACE + ".DB_WRAPPER";
		public static final String PROVINCE_SPINNER_ADAPTER=AppConfigParams.ROOT_NAMESPACE + ".PROVINCE_SPINNER_ADAPTER";
		
		public static final String NEW_CARDS=AppConfigParams.ROOT_NAMESPACE + ".NEW_CARDS";
		
		public static final String PLACE_TYPE=AppConfigParams.ROOT_NAMESPACE + ".PLACE_TYPE";
		public static final String CARD_FILTER_ARRAY=AppConfigParams.ROOT_NAMESPACE + ".CARD_FILTER_ARRAY";
		public static final String SELECTED_PROVINCE_ID=AppConfigParams.ROOT_NAMESPACE + ".SELECTED_PROVINCE_ID";
	}
	
	public static final class PrefKeys{
		public static final String SELECTED_PROVINCE_ID = "SelectedProvinceID";
		public static final String SELECTED_PROVINCE_POS = "SelectedProvincePos";
	}
	
	public static final class Db{
		public static final String ORDER_DESC = " DESC";
		public static final String ORDER_ASC = " ASC";
	}
	
	public static final class ActivityRequestCode{
		public static final int RequestCodeCardSelector = 0;
	}
	
	public static final class CustomResource{
		public static final String SHARED_FILE_NAME = "share.png";
	}
}
