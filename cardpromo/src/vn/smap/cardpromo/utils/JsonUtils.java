package vn.smap.cardpromo.utils;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vn.smap.dbprototype.dao.pojos.Bank;
import vn.smap.dbprototype.dao.pojos.Card;

public class JsonUtils {

	/*
	 * Write an array of Bank to JSON (only write BankID). Return JSON string
	 */
	public static String writeJsonBankIDs(ArrayList<Bank> bankList) {
		JSONArray ja = new JSONArray();
		try {
			for (Bank b : bankList) {
				ja.put(new JSONObject().put("ID", b.getID()));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ja.toString();
	}

	/*
	 * Read from a JSON string and return an array list of IDs
	 */
	public static ArrayList<Integer> readJsonIDs(String jsonStr) {
		ArrayList<Integer> idList = new ArrayList<Integer>();
		JSONArray ja;
		JSONObject jsonObj;
		try {
			ja = new JSONArray(jsonStr);
			for (int i = 0; i < ja.length(); i++) {
				jsonObj = ja.getJSONObject(i);
				idList.add(jsonObj.getInt("ID"));
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return idList;
	}

	/*
	 * Write an array of Card to JSON (only write CardID). Return JSON string
	 */
	public static String writeJsonCardIDs(ArrayList<Card> cardList) {
		JSONArray ja = new JSONArray();
		try {
			for (Card c : cardList) {
				ja.put(new JSONObject().put("ID", c.getID()));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ja.toString();
	}
	
	public static String writeJsonIDs(ArrayList<Integer> idList){
		JSONArray ja = new JSONArray();
		try {
			for (Integer id : idList) {
				ja.put(new JSONObject().put("ID", id));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ja.toString();
	}

}
