package vn.smap.cardpromo.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

import vn.smap.cardpromo.configs.AppConfigParams;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.widget.ArrayAdapter;

public class CardPromoUtils {
	private static File fHome;
	private static File fData;
	private static File fAssets;
	private static File fImages;

	public static File createAppDirStructure() {
		fHome = new File(Environment.getExternalStorageDirectory()
				+ File.separator + AppConfigParams.HOME_APP_DIR_NAME);
		if (!fHome.exists()) {
			fHome.mkdir();

			fData = new File(fHome + File.separator
					+ AppConfigParams.APP_DATA_DIR_NAME);
			fData.mkdir();

			fAssets = new File(fHome + File.separator
					+ AppConfigParams.APP_ASSETS_DIR_NAME);
			fAssets.mkdir();

			fImages = new File(fHome + File.separator
					+ AppConfigParams.APP_ASSETS_DIR_NAME + File.separator
					+ AppConfigParams.APP_ASSETS_IMAGE_DIR_NAME);
			fImages.mkdir();
		}

		return fHome;
	}

	public static File getAppHomeDir() {
		return new File(Environment.getExternalStorageDirectory()
				+ File.separator + AppConfigParams.HOME_APP_DIR_NAME);
	}

	public static File getAppDataDir() {
		return new File(fHome + File.separator
				+ AppConfigParams.APP_DATA_DIR_NAME);
	}

	public static File getAppAssetsDir() {
		return new File(fHome + File.separator
				+ AppConfigParams.APP_ASSETS_DIR_NAME);
	}

	public static File getAppImagesDir() {
		return new File(fHome + File.separator
				+ AppConfigParams.APP_ASSETS_DIR_NAME + File.separator
				+ AppConfigParams.APP_ASSETS_IMAGE_DIR_NAME);
	}

	public static <T> String arrayListToString(ArrayList<T> arr) {
		StringBuffer res = new StringBuffer();
		for (T id : arr) {
			res = res.append(id + "-");
		}
		return res.toString();
	}

	public static Bitmap decodeFile(String f) throws FileNotFoundException {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// The new size we want to scale to
			final int REQUIRED_SIZE = 70;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_SIZE
					&& o.outHeight / scale / 2 >= REQUIRED_SIZE)
				scale *= 2;

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			throw e;
		}
	}

	public static Bitmap decodeStream(InputStream is) {
		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(is, null, o);

		// The new size we want to scale to
		final int REQUIRED_SIZE = 70;

		// Find the correct scale value. It should be the power of 2.
		int scale = 1;
		while (o.outWidth / scale / 2 >= REQUIRED_SIZE
				&& o.outHeight / scale / 2 >= REQUIRED_SIZE)
			scale *= 2;

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeStream(is, null, o2);

	}

	public static <T> void addAllToArrayAdapter(ArrayAdapter<T> dest,
			ArrayList<T> src) throws NullPointerException {
		try {
			for (T item : src) {
				dest.add(item);
			}
		} catch (NullPointerException npe) {
			throw npe;
		}
	}
}
