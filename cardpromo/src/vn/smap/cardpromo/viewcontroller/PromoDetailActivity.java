package vn.smap.cardpromo.viewcontroller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import vn.smap.cardpromo.utils.CardPromoUtils;
import vn.smap.cardpromo.utils.StaticData;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;
import com.googlecode.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_promo_detail)
public class PromoDetailActivity extends SherlockActivity {

	@Extra("selectedPromo")
	FacePromo mFacePromo;

	@ViewById(R.id.btnCall)
	ImageButton btnCall;

	@ViewById(R.id.txvPlaceName)
	TextView txvPlaceName;

	@ViewById(R.id.txvPeriod)
	TextView txvPeriod;

	@ViewById(R.id.txvAddress)
	TextView txvAddress;

	@ViewById(R.id.txvPhone)
	TextView txvPhone;

	@ViewById(R.id.txvPercent)
	TextView txvPercent;

	@ViewById(R.id.txvCards)
	TextView txvCards;

	@ViewById(R.id.txvDescrip)
	TextView txvDescrip;

	@ViewById(R.id.imgAvatar)
	ImageView imgAvatar;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		copyPrivateRawResuorceToPubliclyAccessibleFile();
	}

	@AfterViews
	protected void initViewsValue() {
		// mFacePromo = (FacePromo) getIntent().getSerializableExtra(
		// "selectedPromo");
		txvPlaceName.setText(mFacePromo.getPlaceName());

		try {
			txvPeriod.setText(DateFormat.format("dd-MM-yyyy",
					new SimpleDateFormat("yyyy-MM-dd").parse(mFacePromo
							.getExpireDate())));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		txvAddress.setText(mFacePromo.getPlaceAddress());
		txvPhone.setText(mFacePromo.getPlacePhone());
		txvPercent.setText(String.format("%.1f", mFacePromo.getDiscount())
				+ "%");
		txvCards.setText(mFacePromo.getBankName() + " - "
				+ mFacePromo.getConcatCards());
		txvDescrip.setText(mFacePromo.getConstrain());

		String imgPath = mFacePromo.getPlaceImg();
		Bitmap bmp = null;
		if (imgPath == null || imgPath.length() == 0) {
			switch (mFacePromo.getPlaceTypeID()) {
			case 1:
				imgAvatar.setImageResource(R.drawable.tab_muasam);
				break;
			case 2:
				imgAvatar.setImageResource(R.drawable.tab_thoitrang);
				break;
			case 3:
				imgAvatar.setImageResource(R.drawable.photo_not_available);
				break;
			case 4:
				imgAvatar.setImageResource(R.drawable.tab_amthuc);
				break;
			case 5:
				imgAvatar.setImageResource(R.drawable.tab_dulich);
				break;
			case 6:
				imgAvatar.setImageResource(R.drawable.tab_suckhoe);
				break;
			default:
				imgAvatar.setImageResource(R.drawable.photo_not_available);
				break;
			}

		} else {
			try {
				bmp = CardPromoUtils.decodeFile(CardPromoUtils.getAppHomeDir()
						+ imgPath.substring(imgPath.indexOf(File.separator)));
				imgAvatar.setImageBitmap(bmp);
			} catch (Exception e) {
				imgAvatar.setImageResource(R.drawable.photo_not_available);
			}
		}

	}

	@Click
	protected void btnCall() {
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + mFacePromo.getPlacePhone()));
		startActivity(callIntent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.getSupportMenuInflater().inflate(R.menu.share_action_provider,
				menu);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		// Set file with share history to the provider and set the share intent.
		MenuItem actionItem = menu
				.findItem(R.id.menu_item_share_action_provider_action_bar);
		ShareActionProvider actionProvider = (ShareActionProvider) actionItem
				.getActionProvider();
		actionProvider
				.setShareHistoryFileName(ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
		// Note that you can set/change the intent any time,
		// say when the user has selected an image.
		actionProvider.setShareIntent(createShareIntent());

		return true;
	}

	/**
	 * Creates a sharing {@link Intent}.
	 * 
	 * @return The sharing intent.
	 */
	private Intent createShareIntent() {
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		// Uri uri = null;
		shareIntent.setType("image/*");
		Uri uri = Uri.fromFile(getFileStreamPath("shared.png"));
		// shareIntent.setType("text/plain");

		String shareSubject = "Khuyến mãi chủ thẻ " + mFacePromo.getBankName();
		String shareContent = mFacePromo.getPlaceName() + " khuyến mãi "
				+ mFacePromo.getDiscount() + "% cho thẻ "
				+ mFacePromo.getBankName() + " - "
				+ mFacePromo.getConcatCards();
		// shareIntent.putExtra(Intent.EXTRA_SUBJECT, shareSubject);
		// shareIntent
		// .putExtra(Intent.EXTRA_TEXT,
		// "http://www.google.com");
		shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
		return shareIntent;
	}

	/**
	 * Copies a private raw resource content to a publicly readable file such
	 * that the latter can be shared with other applications.
	 */
	private void copyPrivateRawResuorceToPubliclyAccessibleFile() {
		InputStream inputStream = null;
		FileOutputStream outputStream = null;
		try {
			inputStream = getResources().openRawResource(R.raw.robot);
			outputStream = openFileOutput(
					StaticData.CustomResource.SHARED_FILE_NAME,
					Context.MODE_WORLD_READABLE | Context.MODE_APPEND);
			byte[] buffer = new byte[1024];
			int length = 0;
			try {
				while ((length = inputStream.read(buffer)) > 0) {
					outputStream.write(buffer, 0, length);
				}
			} catch (IOException ioe) {
				/* ignore */
			}
		} catch (FileNotFoundException fnfe) {
			/* ignore */
		} finally {
			try {
				inputStream.close();
			} catch (IOException ioe) {
				/* ignore */
			}
			try {
				outputStream.close();
			} catch (IOException ioe) {
				/* ignore */
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		// more code here for other cases

		}
		return true;
	}
}
