package vn.smap.cardpromo.viewcontroller;

import vn.smap.cardpromo.GlobalData;
import vn.smap.cardpromo.configs.AppConfigParams;
import vn.smap.cardpromo.model.DAOArrayListWrapper;
import vn.smap.cardpromo.utils.JsonUtils;
import vn.smap.cardpromo.viewcontroller.adapters.GridViewCardAdapter;
import vn.smap.cardpromo.viewcontroller.adapters.HorizontalListBankAdapter;
import vn.smap.cardpromo.viewcontroller.adapters.ProvinceSpinnerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.devsmart.android.ui.HorizontalListView;

public class CardSelectorDialogFragment extends DialogFragment implements
		OnItemClickListener, OnItemSelectedListener {
	HorizontalListBankAdapter mAdapter;
	GridViewCardAdapter grvCardAdapter = null;
	ProvinceSpinnerAdapter spnProvinceAdapter = null;
	SparseArray<GridViewCardAdapter> cardAdapterMap = new SparseArray<GridViewCardAdapter>();
	DAOArrayListWrapper dbWrapper;

	// Use this instance of the interface to deliver action events
	CardSelectorDialogListener mListener;

	View dialogView;

	private Spinner spnProvince;

	public CardSelectorDialogFragment(DAOArrayListWrapper dbWrapper,
			ProvinceSpinnerAdapter spnProvinceAdapter) {
		this.dbWrapper = dbWrapper;
		this.spnProvinceAdapter = spnProvinceAdapter;
	}

	/*
	 * The activity that creates an instance of this dialog fragment must
	 * implement this interface in order to receive event callbacks. Each method
	 * passes the DialogFragment in case the host needs to query it.
	 */
	public interface CardSelectorDialogListener {
		public void onDialogPositiveClick(DialogFragment dialog);

		public void onDialogNegativeClick(DialogFragment dialog);
	}

	// Override the Fragment.onAttach() method to instantiate the
	// NoticeDialogListener
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		// Verify that the host activity implements the callback interface
		try {
			// Instantiate the NoticeDialogListener so we can send events to the
			// host
			mListener = (CardSelectorDialogListener) activity;
		} catch (ClassCastException e) {
			// The activity doesn't implement the interface, throw exception
			throw new ClassCastException(activity.toString()
					+ " must implement CardSelectorDialogListener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		mAdapter = new HorizontalListBankAdapter(this.getActivity(),
				dbWrapper.getAllBanks());

		this.dialogView = LayoutInflater.from(this.getActivity()).inflate(
				R.layout.fragment_card_selector, null);

		spnProvince = (Spinner) dialogView.findViewById(R.id.spnProvinces);
		spnProvince.setOnItemSelectedListener(this);
		spnProvince.setAdapter(spnProvinceAdapter);
		spnProvince.setSelection(GlobalData.getSelectedProvincePos());

		HorizontalListView listViewBank = (HorizontalListView) dialogView
				.findViewById(R.id.listviewBank);
		listViewBank.setAdapter(mAdapter);
		listViewBank.setOnItemClickListener(this);

		int curBankID = (int) listViewBank.getSelectedItemId();
		GridViewCardAdapter grvCardAdapter = new GridViewCardAdapter(
				this.getActivity(), dbWrapper.getCardsByBankID(curBankID));
		GridView gridViewCard = (GridView) dialogView
				.findViewById(R.id.gridViewCard);
		gridViewCard.setAdapter(grvCardAdapter);
		gridViewCard.setOnItemClickListener(this);
		this.cardAdapterMap.put(curBankID, grvCardAdapter);

		builder.setView(dialogView);
		builder.setMessage(R.string.title_activity_main)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						GlobalData
								.getSettings()
								.edit()
								.putString(
										AppConfigParams.USER_CARDS_ID_JSON_STRING,
										JsonUtils
												.writeJsonIDs(GlobalData.userCardsIDList))
								.commit();

						GlobalData
								.getSettings()
								.edit()
								.putInt(AppConfigParams.PROVINCE,
										(int) spnProvince.getSelectedItemId())
								.commit();

						mListener
								.onDialogPositiveClick(CardSelectorDialogFragment.this);

					}
				});
		// Create the AlertDialog object and return it
		return builder.create();
	}

	/*
	 * Handle user click on horizontal listview Cards item
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ImageView img;
		switch (parent.getId()) {
		case R.id.listviewBank:
			HorizontalListBankAdapter bankListAdapter = (HorizontalListBankAdapter) parent
					.getAdapter();
			View prevSelectedView = parent.getChildAt(bankListAdapter
					.getCurSelectedIndex());
			if (prevSelectedView != null) {
				((ImageView) prevSelectedView.findViewById(R.id.img))
						.setAlpha(100);
			}
			img = (ImageView) view.findViewById(R.id.img);
			img.setAlpha(255);
			bankListAdapter.setCurSelectedIndex(position);

			int curBankID = (int) bankListAdapter.getItemId(position);
			grvCardAdapter = this.cardAdapterMap.get(curBankID);
			if (grvCardAdapter == null) {
				grvCardAdapter = new GridViewCardAdapter(this.getActivity(),
						dbWrapper.getCardsByBankID(curBankID));
				this.cardAdapterMap.put(curBankID, grvCardAdapter);
			}

			GridView gridViewCard = (GridView) dialogView
					.findViewById(R.id.gridViewCard);
			gridViewCard.setAdapter(grvCardAdapter);
			break;
		case R.id.gridViewCard:

			if (grvCardAdapter != null) {

				CheckBox chk = (CheckBox) view.findViewById(R.id.cbxIsSelected);
				img = (ImageView) view.findViewById(R.id.img);
				Boolean newState = !grvCardAdapter.getChecks(position);
				chk.setChecked(newState);
				grvCardAdapter.setChecksAt(position, newState);
				if (newState) {
					GlobalData.userCardsIDList.add((int) grvCardAdapter
							.getItemId(position));
					img.setAlpha(255);
				} else {
					while (GlobalData.userCardsIDList
							.remove((Integer) (int) grvCardAdapter
									.getItemId(position)))
						;
					img.setAlpha(100);
				}
			}

			break;
		}

	}

	public ProvinceSpinnerAdapter getSpnProvinceAdapter() {
		return spnProvinceAdapter;
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		switch (parent.getId()) {
		case R.id.spnProvinces:
			GlobalData.setSelectedProvincePos(pos);
			break;
		default:
			break;
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}
}
