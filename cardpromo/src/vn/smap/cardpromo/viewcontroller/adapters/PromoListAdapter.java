package vn.smap.cardpromo.viewcontroller.adapters;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import vn.smap.cardpromo.utils.CardPromoUtils;
import vn.smap.cardpromo.viewcontroller.R;
import vn.smap.cardpromo.viewcontroller.SortFilterType;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PromoListAdapter extends ArrayAdapter<FacePromo> {
	private SortFilterType curAdapterSortFilterType;
	private ArrayList<Integer> curCardFilterArray;
	private int curProvinceID;
	private int[] itemColors;

	public PromoListAdapter(Context context, int textViewResourceId,
			List<FacePromo> objects, ArrayList<Integer> curCardFilterArray) {
		super(context, textViewResourceId, objects);
		this.curCardFilterArray = new ArrayList<Integer>();
		this.curCardFilterArray.addAll(curCardFilterArray);
		itemColors = new int[] {
				context.getResources().getColor(R.color.list_promo_row_1),
				context.getResources().getColor(R.color.list_promo_row_2) };

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View itemView = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.item_view_promo, null);
		itemView.setBackgroundColor(itemColors[position % itemColors.length]);

		FacePromo curItem = (FacePromo) this.getItem(position);
		TextView title = (TextView) itemView.findViewById(R.id.txvTitle);
		title.setText(curItem.getPlaceName());

		TextView promoPercent = (TextView) itemView
				.findViewById(R.id.txvPromotePercent);
		promoPercent
				.setText(String.format("%.1f", curItem.getDiscount()) + "%");

		TextView startDate = (TextView) itemView
				.findViewById(R.id.txvSubtitle2);
		startDate.setText("Đ/C: " + curItem.getPlaceAddress());

		TextView endDate = (TextView) itemView.findViewById(R.id.txvSubtitle1);
		try {
			String exp = curItem.getExpireDate();
			if (exp.equalsIgnoreCase("0000-00-00")) {
				exp = "N/A";
			} else {
				exp = (String) DateFormat.format("dd-MM-yyyy",
						(new SimpleDateFormat("yyyy-MM-dd")
								.parse(curItem.getExpireDate())));
			}

			endDate.setText("Đến: " + exp);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		((TextView) itemView.findViewById(R.id.txvCardType)).setText(curItem
				.getBankName() + " - " + curItem.getConcatCards());

		ImageView img = (ImageView) itemView.findViewById(R.id.imgAvatar);
		String imgPath = "";
		Bitmap bmp = null;
		if (imgPath == null || imgPath.length() == 0) {
			switch(curItem.getPlaceTypeID()){
			case 1:
				img.setImageResource(R.drawable.tab_muasam);
				break;
			case 2:
				img.setImageResource(R.drawable.tab_thoitrang);
				break;
			case 3:
				img.setImageResource(R.drawable.photo_not_available);
				break;
			case 4:
				img.setImageResource(R.drawable.tab_amthuc);
				break;
			case 5:
				img.setImageResource(R.drawable.tab_dulich);
				break;
			case 6:
				img.setImageResource(R.drawable.tab_suckhoe);
				break;
			default:
				img.setImageResource(R.drawable.photo_not_available);
				break;
			}
			
		} else {
			try {
				bmp = CardPromoUtils.decodeFile(CardPromoUtils.getAppHomeDir()
						+ imgPath.substring(imgPath.indexOf(File.separator)));
				img.setImageBitmap(bmp);
			} catch (Exception e) {
				img.setImageResource(R.drawable.photo_not_available);
			}
		}

		return itemView;
	}

	public SortFilterType getCurAdapterSortFilterType() {
		return curAdapterSortFilterType;
	}

	public void setCurAdapterSortFilterType(
			SortFilterType curAdapterSortFilterType) {
		this.curAdapterSortFilterType = curAdapterSortFilterType;
	}

	public ArrayList<Integer> getCurCardFilterArray() {
		return curCardFilterArray;
	}

	public void setCurCardFilterArray(ArrayList<Integer> curCardFilterArray) {
		this.curCardFilterArray = new ArrayList<Integer>();
		this.curCardFilterArray.addAll(curCardFilterArray);
	}

	public int getCurProvinceID() {
		return curProvinceID;
	}

	public void setCurProvinceID(int curProvinceID) {
		this.curProvinceID = curProvinceID;
	}

}
