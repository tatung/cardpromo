package vn.smap.cardpromo.viewcontroller.adapters;

import java.util.List;

import vn.smap.cardpromo.viewcontroller.R;
import vn.smap.dbprototype.dao.pojos.Province;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ProvinceSpinnerAdapter extends ArrayAdapter<Province> {
	private int selectedPosition = 0;
	private Context mContext;

	public ProvinceSpinnerAdapter(Context context, int textViewResourceId,
			List<Province> objects) {
		super(context.getApplicationContext(), textViewResourceId, objects);
		mContext = context.getApplicationContext();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View itemView = LayoutInflater.from(mContext).inflate(
				R.layout.simple_spinner_item, null);
		TextView txvProvince = (TextView) itemView
				.findViewById(android.R.id.text1);
		txvProvince.setText(((Province) this.getItem(position)).getName());
		return itemView;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View itemView = LayoutInflater.from(mContext).inflate(
				R.layout.select_dialog_singlechoice, null);
		TextView txvProvince = (TextView) itemView
				.findViewById(android.R.id.text1);
		txvProvince.setText(((Province) this.getItem(position)).getName());
		return itemView;

	}

	public int getSelectedPosition() {
		return selectedPosition;
	}

	public void setSelectedPosition(int selectedPosition) {
		this.selectedPosition = selectedPosition;
	}

}
