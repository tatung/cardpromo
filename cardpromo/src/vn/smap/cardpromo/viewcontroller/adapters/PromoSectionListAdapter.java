package vn.smap.cardpromo.viewcontroller.adapters;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import vn.smap.cardpromo.model.DAOWrapper;
import vn.smap.cardpromo.utils.CardPromoUtils;
import vn.smap.cardpromo.viewcontroller.R;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.format.DateFormat;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PromoSectionListAdapter extends ArrayAdapter<FacePromo> {

	private static final int VIEW_TYPE_GROUP_START = 0;
	private static final int VIEW_TYPE_GROUP_CONT = 1;
	private static final int VIEW_TYPE_COUNT = 2;
	private int[] itemColors;

	private DAOWrapper dbWrapper;
	private SparseArray<String> placeTypeNameMap = new SparseArray<String>();

	public PromoSectionListAdapter(Context context, int textViewResourceId,
			List<FacePromo> objects, DAOWrapper dbWrapper) {
		super(context, textViewResourceId, objects);
		this.dbWrapper = dbWrapper;
		placeTypeNameMap = dbWrapper.getPlaceTypeNameMap();
		itemColors = new int[] {
				context.getResources().getColor(R.color.list_promo_row_1),
				context.getResources().getColor(R.color.list_promo_row_2) };
	}

	@Override
	public int getViewTypeCount() {
		return VIEW_TYPE_COUNT;
	}

	@Override
	public int getItemViewType(int position) {
		// There is always a group header for the first data item

		if (position == 0) {
			return VIEW_TYPE_GROUP_START;
		}

		// For other items, decide based on current data
		boolean newGroup = isNewGroup(position);

		// Check item grouping

		if (newGroup) {
			return VIEW_TYPE_GROUP_START;
		} else {
			return VIEW_TYPE_GROUP_CONT;
		}
	}

	private boolean isNewGroup(int pos) {
		if (pos == 0
				|| this.getItem(pos - 1).getPlaceTypeID() != this.getItem(pos)
						.getPlaceTypeID()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View itemView;
		if (isNewGroup(position)) {
			itemView = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.item_view_promo_with_header, null);
			TextView txvHeader = (TextView) itemView
					.findViewById(R.id.txvMessageItemWhenHeader);
			txvHeader.setText(placeTypeNameMap.get(this.getItem(position)
					.getPlaceTypeID()));
			txvHeader.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
				}
			});

		} else {
			itemView = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.item_view_promo, null);
		}
		itemView.setBackgroundColor(itemColors[position % itemColors.length]);
		FacePromo curItem = this.getItem(position);
		TextView title = (TextView) itemView.findViewById(R.id.txvTitle);
		title.setText(curItem.getPlaceName());

		TextView startDate = (TextView) itemView
				.findViewById(R.id.txvSubtitle2);
		startDate.setText(curItem.getPlaceTypeID() + " Đ/C:  "
				+ curItem.getPlaceAddress());

		TextView endDate = (TextView) itemView.findViewById(R.id.txvSubtitle1);
		try {
			endDate.setText("Đến "
					+ DateFormat.format("dd-MM-yyyy", (new SimpleDateFormat(
							"yyyy-MM-dd").parse(curItem.getExpireDate()))));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		TextView promoPercent = (TextView) itemView
				.findViewById(R.id.txvPromotePercent);
		promoPercent
				.setText(String.format("%.1f", curItem.getDiscount()) + "%");

		((TextView) itemView.findViewById(R.id.txvCardType)).setText(curItem
				.getBankName() + " - " + curItem.getConcatCards());

		ImageView img = (ImageView) itemView.findViewById(R.id.imgAvatar);
		String imgPath = curItem.getPlaceImg();
		Bitmap bmp = null;
		if (imgPath == null || imgPath.length() == 0) {
			switch (curItem.getPlaceTypeID()) {
			case 1:
				img.setImageResource(R.drawable.tab_muasam);
				break;
			case 2:
				img.setImageResource(R.drawable.tab_thoitrang);
				break;
			case 3:
				img.setImageResource(R.drawable.photo_not_available);
				break;
			case 4:
				img.setImageResource(R.drawable.tab_amthuc);
				break;
			case 5:
				img.setImageResource(R.drawable.tab_dulich);
				break;
			case 6:
				img.setImageResource(R.drawable.tab_suckhoe);
				break;
			default:
				img.setImageResource(R.drawable.photo_not_available);
				break;
			}

		} else {
			try {
				bmp = CardPromoUtils.decodeFile(CardPromoUtils.getAppHomeDir()
						+ imgPath.substring(imgPath.indexOf(File.separator)));
				img.setImageBitmap(bmp);
			} catch (Exception e) {
				img.setImageResource(R.drawable.photo_not_available);
			}
		}

		return itemView;
	}
}
