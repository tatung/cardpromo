package vn.smap.cardpromo.viewcontroller.adapters;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

import vn.smap.cardpromo.utils.CardPromoUtils;
import vn.smap.cardpromo.viewcontroller.CardDeleteDialogFragment;
import vn.smap.cardpromo.viewcontroller.R;
import vn.smap.dbprototype.dao.pojos.Card;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;

public class HorizontalListCardAdapter extends ArrayAdapter<Card> implements
		OnClickListener, OnLongClickListener {
	ArrayList<Boolean> checks = new ArrayList<Boolean>();

	public HorizontalListCardAdapter(Context context, ArrayList<Card> cardList) {
		super(context, R.layout.item_view_img_cbx, cardList);
		populateChecks(true, null);

	}

	public void populateChecks(boolean defaultCheckState,
			ArrayList<Integer> newCards) {
		checks.clear();
		for (int i = 0; i < this.getCount(); i++) {
			checks.add(defaultCheckState);
		}
		if (newCards != null && newCards.size() > 0) {
			for (int i = 0; i < this.getCount(); i++) {
				for (int j = 0; j < newCards.size(); j++) {
					if (newCards.get(j) == this.getItemId(i)) {
						checks.set(i, true);
					}
				}
			}
		}
	}

	@Override
	public long getItemId(int position) {
		return this.getItem(position).getID();
	}

	@SuppressWarnings("deprecation")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ItemViewHolder itemViewHolder = null;

		if (convertView == null) {
			convertView = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.item_view_img_cbx, null);
			itemViewHolder = new ItemViewHolder();
			itemViewHolder.chk = (CheckBox) convertView
					.findViewById(R.id.cbxIsSelected);
			itemViewHolder.img = (ImageView) convertView.findViewById(R.id.img);
			itemViewHolder.txvCardName = (TextView) convertView
					.findViewById(R.id.txvName);
			itemViewHolder.imgCheck = (ImageView) convertView
					.findViewById(R.id.imgCheck);
			itemViewHolder.pos = position;
			convertView.setTag(itemViewHolder);
		}

		// convertView.setPressed(false);

		if (itemViewHolder == null) {
			itemViewHolder = (ItemViewHolder) convertView.getTag();
		}

		itemViewHolder.txvCardName.setText(this.getItem(position)
				.getShortName());

		String imgPath = this.getItem(position).getImgPath();
		Bitmap bmp = null;
		if (imgPath == null || imgPath.length() == 0) {
			itemViewHolder.img.setImageResource(R.drawable.photo_not_available);
		} else {
			try {
				bmp = CardPromoUtils.decodeFile(CardPromoUtils.getAppHomeDir()
						+ File.separator + imgPath);
				itemViewHolder.img.setImageBitmap(bmp);
			} catch (Exception e) {
				try {
					InputStream is = this.getContext().getAssets()
							.open(imgPath.substring(imgPath.indexOf("Images")));
					bmp = CardPromoUtils.decodeStream(is);
					itemViewHolder.img.setImageBitmap(bmp);
				} catch (Exception e1) {
					itemViewHolder.img
							.setImageResource(R.drawable.photo_not_available);
				}
			}
		}

		itemViewHolder.chk.setTag(Integer.valueOf(position));
		itemViewHolder.chk.setOnClickListener(this);
		itemViewHolder.chk.setChecked(checks.get(position));
		itemViewHolder.imgCheck
				.setVisibility(checks.get(position) ? View.VISIBLE : View.GONE);
		if (checks.get(position)) {
			itemViewHolder.img.setAlpha(255);
		} else {
			itemViewHolder.img.setAlpha(100);
		}

		convertView.setOnLongClickListener(this);

		// convertView.setOnTouchListener(new OnTouchListener() {
		//
		// @Override
		// public boolean onTouch(View v, MotionEvent event) {
		// switch (event.getAction()) {
		// case MotionEvent.ACTION_DOWN:
		// v.setPressed(true);
		// break;
		//
		// case MotionEvent.ACTION_UP:
		// v.setPressed(false);
		// break;
		// }
		// return false;
		// }
		//
		// });
		return convertView;
	}

	private static class ItemViewHolder {
		CheckBox chk;
		ImageView img;
		ImageView imgCheck;
		TextView txvCardName;
		int pos;
	}

	@Override
	public void onClick(View view) {
		Integer index = (Integer) view.getTag();
		boolean state = checks.get(index);

		checks.set(index, !state);
	}

	public Boolean getChecks(int position) {
		return checks.get(position);
	}

	public void setChecksAt(int pos, Boolean val) {
		this.checks.set(pos, val);
	}

	@Override
	public boolean onLongClick(View v) {
		int idx = ((ItemViewHolder) v.getTag()).pos;
		final int itemID = (int) this.getItemId(idx);
		Card curCard = this.getItem(idx);
		FragmentManager manager = ((SherlockFragmentActivity) this.getContext()).getSupportFragmentManager();
		(new CardDeleteDialogFragment(curCard)).show(manager, "abc");
		return false;
	}

}
