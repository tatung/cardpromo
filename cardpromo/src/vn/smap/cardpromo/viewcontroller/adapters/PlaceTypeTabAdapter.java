package vn.smap.cardpromo.viewcontroller.adapters;

import java.util.ArrayList;

import vn.smap.cardpromo.viewcontroller.ListPageFragment;
import vn.smap.dbprototype.dao.pojos.PlaceType;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

public class PlaceTypeTabAdapter extends FragmentPagerAdapter {
	ArrayList<PlaceType> typeList;
	ArrayList<ListPageFragment> fragList;
	SparseArray<ListPageFragment> fragmentList;
	ArrayList<Integer> cardFilterArray;
	int provinceID;

	public PlaceTypeTabAdapter(FragmentManager fm,
			ArrayList<PlaceType> promoTypeList,
			ArrayList<Integer> cardFilterArray, int provinceID) {
		super(fm);
		this.typeList = promoTypeList;
		this.cardFilterArray = cardFilterArray;
		this.fragList = new ArrayList<ListPageFragment>();
		fragmentList = new SparseArray<ListPageFragment>();
		this.provinceID = provinceID;

	}

	@Override
	public Fragment getItem(int position) {
		if (position >= 0 && position < typeList.size()) {
			ListPageFragment lpf;
			int placeTypeID = typeList.get(position).getID();
			if (fragmentList.get(placeTypeID) == null) {
				lpf = ListPageFragment.newInstance(
						typeList.get(position), cardFilterArray, provinceID);
				fragmentList.put(placeTypeID, lpf);
				this.fragList.add(lpf);
			}
			
			return fragmentList.get(placeTypeID);
		} else {
			System.out.println("Weird-----------------------------");
			return null;
		}
	}
	
	public ListPageFragment getFragment(int placeTypeID){
		return fragmentList.get(placeTypeID);
	}

	public ListPageFragment getPage(int pos) {
		return fragList.get(pos);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return typeList.get(position % typeList.size()).getName();
	}

	@Override
	public int getCount() {
		return typeList.size();
	}
}
