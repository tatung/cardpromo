package vn.smap.cardpromo.viewcontroller.adapters;

import java.util.ArrayList;

import vn.smap.cardpromo.GlobalData;
import vn.smap.cardpromo.configs.AppConfigParams;
import vn.smap.cardpromo.model.DAOArrayListWrapper;
import vn.smap.cardpromo.utils.CardPromoUtils;
import vn.smap.cardpromo.viewcontroller.ListPageFragment;
import vn.smap.cardpromo.viewcontroller.R;
import vn.smap.cardpromo.viewcontroller.SortFilterType;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;

import com.commonsware.cwac.endless.EndlessAdapter;

public class EndlessPromoListAdapter extends EndlessAdapter {
	protected View pendingView;
	protected RotateAnimation rotate;

	private int placeTypeID;
	private ArrayList<Integer> cardIDs;
	private ArrayList<FacePromo> cachedList;

	private SortFilterType sortFilterType;

	PromoListAdapter wrapped;

	public EndlessPromoListAdapter(Context context, PromoListAdapter wrapped,
			int placeTypeID, ArrayList<Integer> cardIDs) {
		super(context, wrapped, R.layout.loading_placeholder);
		this.wrapped = wrapped;
		this.placeTypeID = placeTypeID;
		this.cardIDs = cardIDs;

		rotate = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF,
				0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		rotate.setDuration(600);
		rotate.setRepeatMode(Animation.RESTART);
		rotate.setRepeatCount(Animation.INFINITE);
	}

	public PromoListAdapter getWrappedAdapter() {
		return wrapped;
	}

	@Override
	protected boolean cacheInBackground() throws Exception {
		cachedList = null;
		if (wrapped != null && wrapped.getCount() > 0) {
			// cachedList = DAOArrayListWrapper.getInstance(this.getContext())
			// .getFacePromosBy(placeTypeID, cardIDs,
			// GlobalData.getSelectedProvinceID(),
			// ListPageFragment.getOrderString(),
			// AppConfigParams.LISTVIEW_LIMIT_COUNT,
			// wrapped.getCount());
			cachedList = DAOArrayListWrapper.getInstance(this.getContext())
					.getGroupFacePromos(placeTypeID, cardIDs,
							GlobalData.getSelectedProvinceID(),
							ListPageFragment.getOrderString(),
							AppConfigParams.LISTVIEW_LIMIT_COUNT, wrapped.getCount());
		}
		if (cachedList == null) {
			return false;
		}
		return (cachedList.size() > 0);
	}

	@Override
	protected void appendCachedData() {
		if (cachedList != null) {
			// wrapped.addAll(cachedList);
			CardPromoUtils.addAllToArrayAdapter(wrapped, cachedList);
		}
	}

	@Override
	protected View getPendingView(ViewGroup parent) {
		View row = LayoutInflater.from(parent.getContext()).inflate(
				R.layout.loading_placeholder, null);

		pendingView = row.findViewById(R.id.throbber);
		pendingView.setVisibility(View.VISIBLE);
		startProgressAnimation();

		return (row);
	}

	void startProgressAnimation() {
		if (pendingView != null) {
			pendingView.startAnimation(rotate);
		}
	}

	public SortFilterType getSortFilterType() {
		return sortFilterType;
	}

	public void setSortFilterType(SortFilterType sortFilterType) {
		this.sortFilterType = sortFilterType;
	}

}
