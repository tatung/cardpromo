package vn.smap.cardpromo.viewcontroller.adapters;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

import vn.smap.cardpromo.GlobalData;
import vn.smap.cardpromo.utils.CardPromoUtils;
import vn.smap.cardpromo.viewcontroller.R;
import vn.smap.dbprototype.dao.pojos.Card;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;


public class GridViewCardAdapter extends ArrayAdapter<Card> implements
		OnClickListener {
	Boolean defaultCheckState = false;
	ArrayList<Card> cardList;
	ArrayList<Boolean> checks = new ArrayList<Boolean>();

	public GridViewCardAdapter(Context context, ArrayList<Card> cardList) {
		super(context, R.id.gridViewCard, cardList);
		this.cardList = cardList;
		for (int i = 0; i < cardList.size(); i++) {
			checks.add(defaultCheckState);
		}
	}

	@Override
	public int getCount() {
		return cardList.size();
	}

	@Override
	public Card getItem(int position) {
		return cardList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return cardList.get(position).getID();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.item_view_img_cbx, null);
		}
		Card c = cardList.get(position);
		CheckBox chk = (CheckBox) convertView.findViewById(R.id.cbxIsSelected);
		ImageView img = (ImageView) convertView.findViewById(R.id.img);
		ImageView imgCheck = (ImageView) convertView
				.findViewById(R.id.imgCheck);
		img.setFocusable(false);
		img.setFocusableInTouchMode(false);
		TextView txvCardName = (TextView) convertView
				.findViewById(R.id.txvName);

		txvCardName.setText(c.getShortName());

		String imgPath = c.getImgPath();
		Bitmap bmp = null;
		if (imgPath == null || imgPath.length() == 0) {
			img.setImageResource(R.drawable.photo_not_available);
		} else {
			try {
				bmp = CardPromoUtils.decodeFile(CardPromoUtils.getAppHomeDir()
						+ File.separator + imgPath);
				img.setImageBitmap(bmp);
			} catch (Exception e) {
				try {
					InputStream is = this.getContext().getAssets()
							.open(imgPath.substring(imgPath.indexOf("Images")));
					bmp = CardPromoUtils.decodeStream(is);
					img.setImageBitmap(bmp);
				} catch (Exception e1) {
					img.setImageResource(R.drawable.photo_not_available);
				}
			}
		}

		if (GlobalData.userCardsIDList.contains(c.getID())) {
			checks.set(position, true);
		}

		chk.setTag(Integer.valueOf(position));
		chk.setOnClickListener(this);
		chk.setChecked(checks.get(position));
		imgCheck.setVisibility(checks.get(position) ? View.VISIBLE : View.GONE);
		if (checks.get(position)) {
			img.setAlpha(255);
		} else {
			img.setAlpha(100);
		}
		return convertView;
	}

	@Override
	public void onClick(View view) {
		Integer index = (Integer) view.getTag();
		boolean state = checks.get(index);

		checks.set(index, !state);
	}

	public Boolean getChecks(int position) {
		return checks.get(position);
	}

	public void setChecksAt(int pos, Boolean val) {
		this.checks.set(pos, val);
	}

}
