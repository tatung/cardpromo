package vn.smap.cardpromo.viewcontroller.adapters;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

import vn.smap.cardpromo.utils.CardPromoUtils;
import vn.smap.cardpromo.viewcontroller.R;
import vn.smap.dbprototype.dao.pojos.Bank;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class HorizontalListBankAdapter extends ArrayAdapter<Bank> implements
		OnClickListener {
	boolean defaultCheckState = false;
	ArrayList<Bank> bankList;
	ArrayList<Boolean> checks = new ArrayList<Boolean>();
	int curSelectedIndex;

	int prevSelectedIndex;

	LayoutInflater inflater;

	public HorizontalListBankAdapter(Context context, ArrayList<Bank> bankList) {
		super(context, R.layout.item_view_img_cbx, bankList);
		this.bankList = bankList;

		for (int i = 0; i < bankList.size(); i++) {
			checks.add(defaultCheckState);
		}

		inflater = LayoutInflater.from(this.getContext());
	}

	@Override
	public int getCount() {
		return bankList.size();
	}

	@Override
	public Bank getItem(int position) {
		return bankList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return bankList.get(position).getID();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_view_img_cbx, null);
		}

		CheckBox chk = (CheckBox) convertView.findViewById(R.id.cbxIsSelected);
		// chk.setEnabled(false);
		ImageView img = (ImageView) convertView.findViewById(R.id.img);
		ViewGroup.LayoutParams layoutParams = img.getLayoutParams();
		layoutParams.height = 80;
		layoutParams.width = 120;
		img.setLayoutParams(layoutParams);
		ImageView imgCheck = (ImageView) convertView
				.findViewById(R.id.imgCheck);
		TextView txvName = (TextView) convertView.findViewById(R.id.txvName);
		txvName.setText(bankList.get(position).getShortName());
		// txvName.setText(CardPromoUtils.getBankShortName(bankList.get(position)));

		// TODO: add code for displaying real image of bank
		String imgPath = bankList.get(position).getLogo();
		Bitmap bmp = null;
		if (imgPath == null || imgPath.length() == 0) {
			img.setImageResource(R.drawable.photo_not_available);
		} else {
			try {
				bmp = CardPromoUtils.decodeFile(CardPromoUtils.getAppHomeDir()
						+ File.separator + imgPath);
				img.setImageBitmap(bmp);
			} catch (Exception e) {
				// /CardPromo/Assets/Images/bank/

				try {
					InputStream is = this.getContext().getAssets()
							.open(imgPath.substring(imgPath.indexOf("Images")));
					bmp = CardPromoUtils.decodeStream(is);
					img.setImageBitmap(bmp);
				} catch (Exception e1) {
					img.setImageResource(R.drawable.photo_not_available);
				}

			}
		}

		chk.setTag(Integer.valueOf(position));
		chk.setOnClickListener(this);
		chk.setChecked(checks.get(position));
		// img.setAlpha(50);
		if (position == curSelectedIndex) {
			img.setAlpha(255);
		} else {
			img.setAlpha(40);
		}
		return convertView;
	}

	@Override
	public void onClick(View view) {
		Integer index = (Integer) view.getTag();
		boolean state = checks.get(index);

		checks.set(index, !state);
	}

	public Boolean getChecks(int position) {
		return checks.get(position);
	}

	public void setChecksAt(int pos, Boolean val) {
		this.checks.set(pos, val);
	}

	public int getCurSelectedIndex() {
		return curSelectedIndex;
	}

	public void setCurSelectedIndex(int curSelectedIndex) {
		this.curSelectedIndex = curSelectedIndex;
	}

}
