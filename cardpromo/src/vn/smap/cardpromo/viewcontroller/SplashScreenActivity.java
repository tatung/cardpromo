package vn.smap.cardpromo.viewcontroller;

import vn.smap.cardpromo.model.DAOArrayListWrapper;
import android.app.ProgressDialog;
import android.content.Intent;

import com.actionbarsherlock.app.SherlockActivity;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.UiThread;

@EActivity(R.layout.activity_splash_screen)
public class SplashScreenActivity extends SherlockActivity {
	ProgressDialog progressDialog;

	@UiThread
	protected void showWaitDialog() {
		progressDialog = new ProgressDialog(this);
		this.progressDialog.setMessage(this.getResources().getText(
				R.string.lbl_please_wait));
		this.progressDialog.show();
	}

	@UiThread
	protected void dismissWaitDialog() {
		this.progressDialog.dismiss();
	}

	@AfterViews
	@Background
	protected void initDbConn() {

		showWaitDialog();
		DAOArrayListWrapper.getInstance(this);
		dismissWaitDialog();
		dismissSplashScreen();
	}

	@UiThread
	protected void dismissSplashScreen() {
		finish();
		Intent intent = new Intent(this, MainActivity_.class);
		startActivity(intent);

	}
}
