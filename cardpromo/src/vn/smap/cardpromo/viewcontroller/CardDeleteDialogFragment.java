package vn.smap.cardpromo.viewcontroller;

import vn.smap.cardpromo.GlobalData;
import vn.smap.dbprototype.dao.pojos.Card;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class CardDeleteDialogFragment extends DialogFragment {
	CardDeleteDialogListener mListener;
	final Card curCard;

	public CardDeleteDialogFragment(Card curCard) {
		this.curCard = curCard;
	}

	/*
	 * The activity that creates an instance of this dialog fragment must
	 * implement this interface in order to receive event callbacks. Each method
	 * passes the DialogFragment in case the host needs to query it.
	 */
	public interface CardDeleteDialogListener {
		public void onCardDeleteDialogPositiveClick(DialogFragment dialog);

		public void onCardDeleteDialogNegativeClick(DialogFragment dialog);
	}

	// Override the Fragment.onAttach() method to instantiate the
	// NoticeDialogListener
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		// Verify that the host activity implements the callback interface
		try {
			// Instantiate the NoticeDialogListener so we can send events to the
			// host
			mListener = (CardDeleteDialogListener) activity;
		} catch (ClassCastException e) {
			// The activity doesn't implement the interface, throw exception
			throw new ClassCastException(activity.toString()
					+ " must implement CardSelectorDialogListener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		builder.setTitle(this.getActivity().getText(R.string.lbl_delete_card))
				.setMessage(curCard.getBankName() + " - " + curCard.getShortName() + ". " + 
						this.getActivity()
								.getText(R.string.card_delete_confirm))
				.setNegativeButton(R.string.button_label_CANCEL,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();

							}
						})
				.setPositiveButton(R.string.button_label_OK,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								GlobalData.deleteUserCard(curCard.getID());
								mListener.onCardDeleteDialogPositiveClick(CardDeleteDialogFragment.this);
							}
						});
		// Create the AlertDialog object and return it
		return builder.create();
	}
}
