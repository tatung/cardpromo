package vn.smap.cardpromo.viewcontroller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import vn.smap.cardpromo.GlobalData;
import vn.smap.cardpromo.model.CardPromoSearchEngine;
import vn.smap.cardpromo.model.DAOArrayListWrapper;
import vn.smap.cardpromo.model.DAOWrapper;
import vn.smap.cardpromo.utils.StaticData;
import vn.smap.cardpromo.viewcontroller.adapters.PromoSectionListAdapter;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_search_result)
public class SearchResultActivity extends SherlockListActivity {
	@ViewById(R.id.txvKeywords)
	protected TextView txvKeywords;

	private DAOWrapper dbWrapper;

	@Extra(StaticData.ExtraKeys.KEYWORDS)
	String keywords;
	
	private PromoSectionListAdapter mAdapter;

	private ProgressDialog progressDialog;

	@AfterViews
	protected void initViewsValue() {
		this.txvKeywords.setText("\"" + keywords + "\"");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.dbWrapper = DAOArrayListWrapper.getInstance(this);
		this.searchPromo();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Used to put dark icons on light action bar
		boolean isLight = false;

		// Create the search view
		SearchView searchView = new SearchView(getSupportActionBar()
				.getThemedContext());
		searchView.setQueryHint(this.getResources().getString(
				R.string.search_hint));
		searchView.setOnQueryTextListener(new SearchViewListener(this, this
				.getSelectedCardIDs()));

		menu.add("Search")
				.setIcon(
						isLight ? R.drawable.ic_search_inverse
								: R.drawable.abs__ic_search)
				.setActionView(searchView)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);

		this.getSupportMenuInflater().inflate(R.menu.activity_main, menu);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		menu.findItem(R.id.menu_provinces).setVisible(false);

		return true;
	}

	private void populateResultListView(ArrayList<FacePromo> res) {
		mAdapter = new PromoSectionListAdapter(SearchResultActivity.this,
				R.id.lblListDescrip, res, this.dbWrapper);
		setListAdapter(mAdapter);
		this.txvKeywords.setText("\"" + this.keywords + "\"");
	}

	private ArrayList<Integer> getSelectedCardIDs() {
		return GlobalData.userCardsIDList;
	}

	private class SearchViewListener implements OnQueryTextListener {
		Context mCtx;
		ArrayList<Integer> cardIDs;

		public SearchViewListener(Context ctx, ArrayList<Integer> cardIDs) {
			this.mCtx = ctx;
			this.cardIDs = cardIDs;
		}

		@Override
		public boolean onQueryTextSubmit(String query) {
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
			SearchResultActivity.this.keywords = query;
			if (keywords.length() >= 3) {
				// new SearchQueryWorker(SearchResultActivity.this,
				// this.dbWrapper, query, cardIDs).execute();
				SearchResultActivity.this.searchPromo();
			} else {
				new AlertDialog.Builder(mCtx)
						.setTitle(mCtx.getText(R.string.title_search_result))
						.setMessage(
								mCtx.getText(R.string.msg_too_short_keyword))
						.setPositiveButton(R.string.button_label_OK,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								}).show();
			}
			return false;
		}

		@Override
		public boolean onQueryTextChange(String newText) {
			// TODO Auto-generated method stub
			return false;
		}

	}

	@UiThread
	protected void showWaitDialog() {
		progressDialog = new ProgressDialog(this);
		this.progressDialog.setMessage(this.getResources().getText(
				R.string.lbl_searching));
		this.progressDialog.show();
	}

	@UiThread
	protected void dismissWaitDialog() {
		this.progressDialog.dismiss();
	}

	@Background
	protected void searchPromo() {
		showWaitDialog();
		ArrayList<FacePromo> res = CardPromoSearchEngine.getInstance(this)
				.searchPromo(keywords, null, this.getSelectedCardIDs(), null);
		Collections.sort(res, new Comparator<FacePromo>() {
			@Override
			public int compare(FacePromo lhs, FacePromo rhs) {
				return ((Integer) lhs.getPlaceTypeID()).compareTo(rhs
						.getPlaceTypeID());
			}

		});
		dismissWaitDialog();
		showSearchResult(res);
	}

	@UiThread
	protected void showSearchResult(ArrayList<FacePromo> res) {
		populateResultListView(res);
	}

	// private class SearchQueryWorker extends
	// AsyncTask<Void, Void, ArrayList<FacePromo>> {
	// private DAOWrapper dbWrapper;
	// private Context mContext;
	// private String keywords;
	// private ArrayList<Integer> cardIDs;
	// private ProgressDialog progressDialog;
	//
	// public SearchQueryWorker(Context context, DAOWrapper dbWrapper,
	// String keywords, ArrayList<Integer> cardIDs) {
	// this.mContext = context;
	// this.dbWrapper = dbWrapper;
	// this.keywords = keywords;
	// this.cardIDs = cardIDs;
	// }
	//
	// @Override
	// protected void onPreExecute() {
	// progressDialog = new ProgressDialog(mContext);
	// this.progressDialog.setMessage(mContext.getResources().getText(
	// R.string.lbl_searching));
	// this.progressDialog.show();
	// }
	//
	// @Override
	// protected ArrayList<FacePromo> doInBackground(Void... params) {
	// ArrayList<FacePromo> res = CardPromoSearchEngine.getInstance(
	// mContext).searchPromo(keywords);
	// Collections.sort(res, new Comparator<FacePromo>() {
	// @Override
	// public int compare(FacePromo lhs, FacePromo rhs) {
	// return ((Integer) lhs.getPlaceTypeID()).compareTo(rhs
	// .getPlaceTypeID());
	// }
	//
	// });
	// return res;
	// }
	//
	// @Override
	// protected void onPostExecute(ArrayList<FacePromo> res) {
	// this.progressDialog.dismiss();
	// populateResultListView(res);
	// }
	// }

	@Override
	public void onListItemClick(ListView l, View v, int pos, long id) {
		Intent promoDetailIntent = new Intent(this, PromoDetailActivity_.class);
		promoDetailIntent.putExtra("selectedPromo", mAdapter.getItem(pos));
		startActivity(promoDetailIntent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		// more code here for other cases

		}
		return true;
	}
}
