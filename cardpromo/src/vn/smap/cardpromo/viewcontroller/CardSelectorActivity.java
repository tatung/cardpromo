package vn.smap.cardpromo.viewcontroller;

import java.util.ArrayList;

import vn.smap.cardpromo.GlobalData;
import vn.smap.cardpromo.model.DAOArrayListWrapper;
import vn.smap.cardpromo.utils.StaticData;
import vn.smap.cardpromo.viewcontroller.adapters.GridViewCardAdapter;
import vn.smap.cardpromo.viewcontroller.adapters.HorizontalListBankAdapter;
import vn.smap.cardpromo.viewcontroller.providers.actionproviders.CardPromoProvincesSpinnerActionProvider;
import vn.smap.cardpromo.viewcontroller.providers.actionproviders.CardPromoProvincesSpinnerActionProvider.ProvinceSelectedListener;
import android.content.Intent;
import android.text.Html;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.devsmart.android.ui.HorizontalListView;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ViewById;

@EActivity(R.layout.fragment_card_selector)
public class CardSelectorActivity extends SherlockActivity implements
		OnItemSelectedListener, OnItemClickListener, ProvinceSelectedListener {

	@ViewById(R.id.listviewBank)
	HorizontalListView listViewBank;

	@ViewById(R.id.gridViewCard)
	GridView gridViewCard;

	@ViewById(R.id.btnOK)
	Button btnOK;

	HorizontalListBankAdapter mAdapter;
	GridViewCardAdapter grvCardAdapter = null;
	// ProvinceSpinnerAdapter spnProvinceAdapter = null;
	SparseArray<GridViewCardAdapter> cardAdapterMap = new SparseArray<GridViewCardAdapter>();
	DAOArrayListWrapper dbWrapper;
	ArrayList<Integer> newCards = new ArrayList<Integer>();

	CardPromoProvincesSpinnerActionProvider spnProvinceActionProvider;

	@ViewById(R.id.sclEmptyGridViewCard_Guide)
	View sclEmptyGridViewCard_Guide;
	
	@ViewById(R.id.txvGuide)
	TextView txvGuide;

	public CardSelectorActivity() {
		this.dbWrapper = DAOArrayListWrapper.getInstance(this);

		// this.spnProvinceAdapter = GlobalData.getProvinceSpinnerAdapter();
	}

	@AfterViews
	protected void initViewsValue() {
		mAdapter = new HorizontalListBankAdapter(this, dbWrapper.getAllBanks());
		listViewBank.setAdapter(mAdapter);
		listViewBank.setOnItemClickListener(this);
		int curBankID = (int) listViewBank.getSelectedItemId();
		GridViewCardAdapter grvCardAdapter = new GridViewCardAdapter(this,
				dbWrapper.getCardsByBankID(curBankID));
		gridViewCard.setAdapter(grvCardAdapter);
		// gridViewCard.setEmptyView(LayoutInflater.from(this).inflate(
		// R.layout.instruction_view, null));
		gridViewCard.setOnItemClickListener(this);
		txvGuide
				.setText(Html
						.fromHtml("<b>Hướng dẫn:</b><br/>"
								+ "<b>Bước 1.</b> Chọn tỉnh thành<br/>"
								+ "<b>Bước 2.</b> Chọn ngân hàng chủ thẻ (có thể chọn nhiều ngân hàng)<br/>"
								+ "<b>Bước 3.</b> Chọn thẻ của ngân hàng (có thể chọn nhiều thẻ của nhiều ngân hàng)<br/>"
								+ "<b>Bước 4.</b> Click Tìm điểm ưu đãi<br/>"
								+ "<b>Bước 5.</b> Nếu muốn chọn thêm ngân hàng hay thẻ, thì click nút chọn thẻ<br/>"));
		gridViewCard.setEmptyView(sclEmptyGridViewCard_Guide);
		this.cardAdapterMap.put(curBankID, grvCardAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		this.getSupportMenuInflater().inflate(R.menu.activity_main, menu);
		spnProvinceActionProvider = new CardPromoProvincesSpinnerActionProvider(
				this, this);
		menu.findItem(R.id.menu_provinces).setActionProvider(
				spnProvinceActionProvider);
		return true;
	}

	/*
	 * Handle user click on horizontal listview Cards item
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ImageView img;
		ImageView imgCheck;
		switch (parent.getId()) {
		case R.id.listviewBank:
			HorizontalListBankAdapter bankListAdapter = (HorizontalListBankAdapter) parent
					.getAdapter();
			View prevSelectedView = parent.getChildAt(bankListAdapter
					.getCurSelectedIndex());
			if (prevSelectedView != null) {
				((ImageView) prevSelectedView.findViewById(R.id.img))
						.setAlpha(100);
			}
			img = (ImageView) view.findViewById(R.id.img);
			img.setAlpha(255);
			bankListAdapter.setCurSelectedIndex(position);

			int curBankID = (int) bankListAdapter.getItemId(position);
			grvCardAdapter = this.cardAdapterMap.get(curBankID);
			if (grvCardAdapter == null) {
				grvCardAdapter = new GridViewCardAdapter(this,
						dbWrapper.getCardsByBankID(curBankID));
				this.cardAdapterMap.put(curBankID, grvCardAdapter);
			}
			gridViewCard.setAdapter(grvCardAdapter);
			break;
		case R.id.gridViewCard:

			if (grvCardAdapter != null) {

				CheckBox chk = (CheckBox) view.findViewById(R.id.cbxIsSelected);
				img = (ImageView) view.findViewById(R.id.img);
				imgCheck = (ImageView) view.findViewById(R.id.imgCheck);

				Boolean newState = !grvCardAdapter.getChecks(position);
				chk.setChecked(newState);
				imgCheck.setVisibility(newState ? View.VISIBLE : View.GONE);
				grvCardAdapter.setChecksAt(position, newState);

				int curItemId = (int) grvCardAdapter.getItemId(position);
				if (newState) {
					GlobalData.userCardsIDList.add(curItemId);
					newCards.add(curItemId);
					img.setAlpha(255);
				} else {
					while (GlobalData.userCardsIDList
							.remove((Integer) curItemId))
						;
					while (newCards.remove((Integer) curItemId))
						;
					img.setAlpha(100);
				}

			}

			break;
		}

	}

	// public ProvinceSpinnerAdapter getSpnProvinceAdapter() {
	// return spnProvinceAdapter;
	// }

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		switch (parent.getId()) {
		case R.id.spnProvinces:
			GlobalData.setSelectedProvincePos(pos);
			break;
		default:
			break;
		}

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

	@Click
	void btnOK() {

		Intent returnIntent = new Intent();
		returnIntent.putExtra(StaticData.ExtraKeys.NEW_CARDS, newCards);
		setResult(RESULT_OK, returnIntent);

		finish();
	}

	@Override
	public void onProvinceSelected(int pos) {
		GlobalData.setSelectedProvincePos(pos);

	}
}
