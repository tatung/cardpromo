package vn.smap.cardpromo.viewcontroller;

import java.util.ArrayList;

import vn.smap.cardpromo.GlobalData;
import vn.smap.cardpromo.configs.AppConfigParams;
import vn.smap.cardpromo.model.DAOArrayListWrapper;
import vn.smap.cardpromo.utils.CardPromoUtils;
import vn.smap.cardpromo.utils.JsonUtils;
import vn.smap.cardpromo.utils.StaticData;
import vn.smap.cardpromo.viewcontroller.adapters.HorizontalListCardAdapter;
import vn.smap.cardpromo.viewcontroller.adapters.PlaceTypeTabAdapter;
import vn.smap.cardpromo.viewcontroller.adapters.ProvinceSpinnerAdapter;
import vn.smap.cardpromo.viewcontroller.providers.actionproviders.CardPromoProvincesSpinnerActionProvider;
import vn.smap.cardpromo.viewcontroller.providers.actionproviders.CardPromoProvincesSpinnerActionProvider.ProvinceSelectedListener;
import vn.smap.dbprototype.dao.pojos.Card;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import vn.smap.dbprototype.dao.pojos.PlaceType;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.devsmart.android.ui.HorizontalListView;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.ViewById;
import com.makeramen.segmented.SegmentedRadioGroup;
import com.viewpagerindicator.TabPageIndicator;

@EActivity(R.layout.activity_main)
public class MainActivity extends SherlockFragmentActivity implements
		OnCheckedChangeListener, OnPageChangeListener, OnItemClickListener,
		ProvinceSelectedListener,
		CardDeleteDialogFragment.CardDeleteDialogListener {

	@ViewById(R.id.listview)
	HorizontalListView listview;

	@ViewById(R.id.btnCardConfig)
	Button btnCardConfig;

	// View page indicator
	@ViewById(R.id.indicator)
	TabPageIndicator indicator;

	// Pager and pager adapter
	@ViewById(R.id.pager)
	ViewPager pager;

	@ViewById(R.id.segment_sort_filter)
	SegmentedRadioGroup segmentSortFilter;

	private ArrayList<PlaceType> placeTypeList;
	private FragmentPagerAdapter pageAdapter;

	boolean isFirstLaunchFlag = false;

	DAOArrayListWrapper dbWrapper;

	// Values and adapter for horizontal list of cards
	private ArrayList<Card> cardList;
	private ArrayList<Integer> selectedCardIDs = new ArrayList<Integer>();
	private HorizontalListCardAdapter cardListAdapter;

	// Province spinner action provider
	private CardPromoProvincesSpinnerActionProvider spnProvinceActionProvider = null;

	private SortListPageFragments asynTaskSortListPageFragment;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		CardPromoUtils.createAppDirStructure();

	}

	ProgressDialog progressDialog;

	@UiThread
	protected void showWaitDialog() {
		progressDialog = new ProgressDialog(this);
		this.progressDialog.setMessage(this.getResources().getText(
				R.string.lbl_searching));
		this.progressDialog.show();
	}

	@UiThread
	protected void dismissWaitDialog() {
		this.progressDialog.dismiss();
	}

	@AfterViews
	void initViewsValue() {
		GlobalData.init(this
				.getSharedPreferences(AppConfigParams.PREFS_NAME, 0));
		isFirstLaunchFlag = GlobalData.getSettings().getBoolean(
				AppConfigParams.IS_FIRST_LAUNCH, true);

		ProvinceSpinnerAdapter provinceAdapter;
		provinceAdapter = new ProvinceSpinnerAdapter(
				this.getApplicationContext(), 0, DAOArrayListWrapper
						.getInstance(this).getAllProvinces());
		GlobalData.setProvinceArrayAdapter(provinceAdapter);

		dbWrapper = DAOArrayListWrapper.getInstance(this);

		cardList = dbWrapper.getCardsByCardIDs(GlobalData.userCardsIDList);
		placeTypeList = dbWrapper.getAllPlaceTypes();

		cardListAdapter = new HorizontalListCardAdapter(this, cardList);
		listview.setAdapter(cardListAdapter);
		listview.setOnItemClickListener(this);

		// Initialize data for selectedCardIDs
		populateSelectedCardIDsData();

		indicator.setOnPageChangeListener(this);

		pageAdapter = new PlaceTypeTabAdapter(getSupportFragmentManager(),
				placeTypeList, this.getSelectedCardIDs(),
				GlobalData.getSelectedProvinceID());

		pager.setAdapter(pageAdapter);

		segmentSortFilter.setOnCheckedChangeListener(this);
		switch (segmentSortFilter.getCheckedRadioButtonId()) {
		case R.id.radiobutton_newest:
			ListPageFragment.setSortFilterType(SortFilterType.NEWEST);
			break;
		case R.id.radiobutton_best:
			ListPageFragment.setSortFilterType(SortFilterType.BEST);
			break;
		case R.id.radiobutton_almost_done:
			ListPageFragment.setSortFilterType(SortFilterType.ALMOST_DONE);
			break;
		}
		indicator.setViewPager(pager);

		if (isFirstLaunchFlag) {
			Editor edit = GlobalData.getSettings().edit();
			edit.putBoolean(AppConfigParams.IS_FIRST_LAUNCH, false).commit();
		}

	}

	@Click
	void btnCardConfig() {
		Intent cardConfigIntent = new Intent(MainActivity.this,
				CardSelectorActivity_.class);
		startActivityForResult(cardConfigIntent,
				StaticData.ActivityRequestCode.RequestCodeCardSelector);
	}

	@Override
	public void onPostResume() {
		super.onPostResume();
		// If is first launch time, show card config screen
		if (isFirstLaunchFlag) {
			Intent cardConfigIntent = new Intent(MainActivity.this,
					CardSelectorActivity_.class);
			startActivityForResult(cardConfigIntent,
					StaticData.ActivityRequestCode.RequestCodeCardSelector);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Used to put dark icons on light action bar
		boolean isLight = false;
		// Create the search view
		SearchView searchView = new SearchView(getSupportActionBar()
				.getThemedContext());
		searchView.setQueryHint(this.getResources().getString(
				R.string.search_hint));
		searchView.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String query) {
				if (query.length() >= 3) {
					Intent searchResIntent = new Intent(MainActivity.this,
							SearchResultActivity_.class);
					searchResIntent.putExtra(StaticData.ExtraKeys.KEYWORDS,
							query);
					startActivity(searchResIntent);
				} else {
					new AlertDialog.Builder(MainActivity.this)
							.setTitle(
									MainActivity.this
											.getText(R.string.title_search_result))
							.setMessage(
									MainActivity.this
											.getText(R.string.msg_too_short_keyword))
							.setPositiveButton(R.string.button_label_OK,
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.dismiss();
										}
									}).show();
				}

				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				// TODO Auto-generated method stub
				return false;
			}

		});

		menu.add("Search")
				.setIcon(
						isLight ? R.drawable.ic_search_inverse
								: R.drawable.abs__ic_search)
				.setActionView(searchView)
				.setShowAsAction(
						MenuItem.SHOW_AS_ACTION_IF_ROOM
								| MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);

		this.getSupportMenuInflater().inflate(R.menu.activity_main, menu);
		if (spnProvinceActionProvider == null) {
			spnProvinceActionProvider = new CardPromoProvincesSpinnerActionProvider(
					this, this);
		}
		menu.findItem(R.id.menu_provinces).setActionProvider(
				spnProvinceActionProvider);

		return true;
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		if (group == segmentSortFilter) {
			// set sort filter type to ListPageFragment
			switch (checkedId) {
			case R.id.radiobutton_newest:
				ListPageFragment.setSortFilterType(SortFilterType.NEWEST);
				break;
			case R.id.radiobutton_best:
				ListPageFragment.setSortFilterType(SortFilterType.BEST);
				break;
			case R.id.radiobutton_almost_done:
				ListPageFragment.setSortFilterType(SortFilterType.ALMOST_DONE);
				break;
			}

			int curPageIndex = pager.getCurrentItem();
			((ListPageFragment) ((PlaceTypeTabAdapter) pageAdapter)
					.getItem(curPageIndex)).reloadData();
		}

	}

	@Override
	public void onPageScrollStateChanged(int newState) {
		switch (newState) {
		case ViewPager.SCROLL_STATE_DRAGGING:
			// if (asynTaskSortListPageFragment != null
			// && asynTaskSortListPageFragment.getStatus() != Status.FINISHED) {
			// asynTaskSortListPageFragment.cancel(true);
			// }
			break;
		case ViewPager.SCROLL_STATE_IDLE:
			int curPageIndex = pager.getCurrentItem();
			((ListPageFragment) ((PlaceTypeTabAdapter) pageAdapter)
					.getItem(curPageIndex)).reloadData();

			break;
		}

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int position) {
	}

	private class SortListPageFragments extends
			AsyncTask<Void, Void, ArrayList<FacePromo>> {
		int curPageIndex;
		ListPageFragment lpf;

		@Override
		protected ArrayList<FacePromo> doInBackground(Void... params) {
			curPageIndex = pager.getCurrentItem();
			lpf = ((PlaceTypeTabAdapter) pageAdapter).getPage(curPageIndex);
			return lpf.dataCheckAndReload();
		}

		@Override
		protected void onPostExecute(ArrayList<FacePromo> a) {
			lpf.sortListPromo(a);
		}

	}

	private class ListPageFragmentsRefresh extends
			AsyncTask<Void, Void, ArrayList<FacePromo>> {
		int curPageIndex;
		ListPageFragment lpf;

		@Override
		protected ArrayList<FacePromo> doInBackground(Void... params) {
			curPageIndex = pager.getCurrentItem();
			return null;
		}

		@Override
		protected void onPostExecute(ArrayList<FacePromo> a) {
			lpf = (ListPageFragment) pageAdapter.instantiateItem(pager,
					curPageIndex);

			a = DAOArrayListWrapper.getInstance(MainActivity.this)
					.getGroupFacePromos(lpf.getPlaceTypeID(),
							lpf.getCardFilterArray(),
							GlobalData.getSelectedProvinceID(),
							ListPageFragment.getOrderString(),
							AppConfigParams.LISTVIEW_LIMIT_COUNT, 0);
			lpf.refresh(a);
		}

	}

	/*
	 * Handle user click on horizontal listview Cards item
	 */
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long id) {
		// view.setPressed(!view.isPressed()); // Toggle between colors of the
		// view

		CheckBox chk = (CheckBox) view.findViewById(R.id.cbxIsSelected);
		ImageView img = (ImageView) view.findViewById(R.id.img);
		ImageView imgCheck = (ImageView) view.findViewById(R.id.imgCheck);

		Boolean newState = !cardListAdapter.getChecks(position);
		chk.setChecked(newState);
		imgCheck.setVisibility(newState ? View.VISIBLE : View.GONE);
		cardListAdapter.setChecksAt(position, newState);

		if (newState) {
			img.setAlpha(255);
		} else {
			img.setAlpha(100);
		}

		// view.setPressed(false);
		populateSelectedCardIDsData();
		ListPageFragment.setCardFilterArray(selectedCardIDs);
		new ListPageFragmentsRefresh().execute();

	}

	private void populateSelectedCardIDsData() {
		selectedCardIDs.clear();
		for (int i = 0; i < cardListAdapter.getCount(); i++) {
			if (cardListAdapter.getChecks(i)) {
				selectedCardIDs.add(cardListAdapter.getItem(i).getID());
			}
		}
	}

	public ArrayList<Integer> getSelectedCardIDs() {
		return selectedCardIDs;
	}

	/**
	 * Listener for card selector activity return
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == StaticData.ActivityRequestCode.RequestCodeCardSelector) {
			switch (resultCode) {
			case RESULT_OK:
				GlobalData
						.getSettings()
						.edit()
						.putString(
								AppConfigParams.USER_CARDS_ID_JSON_STRING,
								JsonUtils
										.writeJsonIDs(GlobalData.userCardsIDList))
						.commit();
				// filter by card list
				ArrayList<Integer> newCards = data
						.getIntegerArrayListExtra(StaticData.ExtraKeys.NEW_CARDS);
				cardListAdapter.clear();
				CardPromoUtils
						.addAllToArrayAdapter(cardListAdapter, dbWrapper
								.getCardsByCardIDs(GlobalData.userCardsIDList));
				boolean checkState;
				if (newCards == null || newCards.size() == 0) {
					checkState = true;
				} else {
					checkState = false;
				}
				cardListAdapter.populateChecks(checkState, newCards);
				this.populateSelectedCardIDsData();
				ListPageFragment.setCardFilterArray(selectedCardIDs);
				cardListAdapter.notifyDataSetChanged();

				if (isFirstLaunchFlag) {
					ListPageFragment lpf = (ListPageFragment) pageAdapter
							.instantiateItem(pager, pager.getCurrentItem());
					lpf.setRefreshOnAttach(true);
					isFirstLaunchFlag = false;
				} else {
					new ListPageFragmentsRefresh().execute();
				}
				// filter by province
				this.spnProvinceActionProvider.setSelectedPosition(GlobalData
						.getSelectedProvincePos());
				break;
			case RESULT_CANCELED:
				break;
			}
		}
	}

	@Override
	public void onProvinceSelected(int pos) {
		GlobalData.setSelectedProvincePos(pos);
		ListPageFragment.setProvinceID(GlobalData.getSelectedProvinceID());
		new ListPageFragmentsRefresh().execute();
	}

	@Override
	public void onCardDeleteDialogPositiveClick(DialogFragment dialog) {
		System.out.println("Press OK delete card");
		
		cardListAdapter.clear();
		CardPromoUtils.addAllToArrayAdapter(cardListAdapter,
				dbWrapper.getCardsByCardIDs(GlobalData.userCardsIDList));
		
		cardListAdapter.populateChecks(true, null);
		this.populateSelectedCardIDsData();
		ListPageFragment.setCardFilterArray(selectedCardIDs);
		cardListAdapter.notifyDataSetChanged();

		int curPageIndex = pager.getCurrentItem();
		((ListPageFragment) ((PlaceTypeTabAdapter) pageAdapter)
				.getItem(curPageIndex)).reloadData();

	}

	@Override
	public void onCardDeleteDialogNegativeClick(DialogFragment dialog) {
		// TODO Auto-generated method stub

	}

}
