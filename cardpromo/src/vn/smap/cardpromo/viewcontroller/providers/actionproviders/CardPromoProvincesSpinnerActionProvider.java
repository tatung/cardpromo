package vn.smap.cardpromo.viewcontroller.providers.actionproviders;

import vn.smap.cardpromo.GlobalData;
import vn.smap.cardpromo.viewcontroller.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

import com.actionbarsherlock.view.ActionProvider;

public class CardPromoProvincesSpinnerActionProvider extends ActionProvider
		implements OnItemSelectedListener {

//	private static CardPromoProvincesSpinnerActionProvider mInstance = null;

	private Context mContext;
	private ProvinceSelectedListener mListener;
	private Spinner mSpinner;

	public CardPromoProvincesSpinnerActionProvider(Context context) {
		super(context);
		mContext = context;
	}

	public CardPromoProvincesSpinnerActionProvider(Context context,
			ProvinceSelectedListener listener) {
		super(context);
		mContext = context;
		mListener = listener;
	}

//	public static CardPromoProvincesSpinnerActionProvider getInstance(
//			Context context) {
//		if (mInstance == null) {
//			mInstance = new CardPromoProvincesSpinnerActionProvider(context);
//		} else {
//			mInstance.setListener(null);
//			mInstance.setContext(context);
//		}
//		return mInstance;
//	}
//
//	
//
//	public static CardPromoProvincesSpinnerActionProvider getInstance(
//			Context context, ProvinceSelectedListener listener) {
//		if (mInstance == null) {
//			mInstance = new CardPromoProvincesSpinnerActionProvider(context,
//					listener);
//		} else {
//			mInstance.setListener(listener);
//		}
//		return mInstance;
//	}

	@Override
	public View onCreateActionView() {
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.menu_setting_item, null);
		mSpinner = (Spinner) view.findViewById(R.id.spnProvinces);
		mSpinner.setAdapter(GlobalData.getProvinceArrayAdapter());
		mSpinner.setSelection(GlobalData.getSelectedProvincePos());
		mSpinner.setOnItemSelectedListener(this);

		return view;
	}

	public Spinner getmSpinner() {
		return mSpinner;
	}

	public interface ProvinceSelectedListener {
		public void onProvinceSelected(int provinceID);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		// ((ProvinceSpinnerAdapter) mSpinner.getAdapter())
		// .setSelectedPosition(pos);
		mSpinner.setSelection(pos);
		// Fire event ProvinceSelected
		mListener.onProvinceSelected(pos);

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	public void setSelectedPosition(int pos) {
		try {
			mSpinner.setSelection(pos);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ProvinceSelectedListener getListener() {
		return mListener;
	}

	public void setListener(ProvinceSelectedListener mListener) {
		this.mListener = mListener;
	}

}
