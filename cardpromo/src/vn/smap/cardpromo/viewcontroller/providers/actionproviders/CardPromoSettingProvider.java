package vn.smap.cardpromo.viewcontroller.providers.actionproviders;

import vn.smap.cardpromo.viewcontroller.R;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;

import com.actionbarsherlock.view.ActionProvider;

public class CardPromoSettingProvider extends ActionProvider {
	private final Context mContext;
	private static final Intent sSettingsIntent = new Intent(Settings.ACTION_SETTINGS);

	public CardPromoSettingProvider(Context context) {
		super(context);
		this.mContext = context;
	}

	@Override
	public View onCreateActionView() {
		View view = LayoutInflater.from(mContext).inflate(R.layout.menu_setting_item, null);
//		(view.findViewById(R.id.btnSetting)).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v) {
//				mContext.startActivity(sSettingsIntent);
//			}
//			
//		});
//
		return view;
	}

}
