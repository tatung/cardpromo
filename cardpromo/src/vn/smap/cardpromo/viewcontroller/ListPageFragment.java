package vn.smap.cardpromo.viewcontroller;

import java.util.ArrayList;

import vn.smap.cardpromo.GlobalData;
import vn.smap.cardpromo.configs.AppConfigParams;
import vn.smap.cardpromo.model.DAOArrayListWrapper;
import vn.smap.cardpromo.utils.CardPromoUtils;
import vn.smap.cardpromo.utils.StaticData;
import vn.smap.cardpromo.viewcontroller.adapters.EndlessPromoListAdapter;
import vn.smap.cardpromo.viewcontroller.adapters.PromoListAdapter;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import vn.smap.dbprototype.dao.pojos.PlaceType;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EFragment;
import com.googlecode.androidannotations.annotations.FragmentArg;
import com.googlecode.androidannotations.annotations.UiThread;

@EFragment(R.layout.fragment_list_page)
public class ListPageFragment extends SherlockListFragment {
	private static SparseArray<ArrayList<FacePromo>> promoListDataSourceMap = new SparseArray<ArrayList<FacePromo>>();
	private static SortFilterType sortFilterType;

	@FragmentArg(StaticData.ExtraKeys.CARD_FILTER_ARRAY)
	static ArrayList<Integer> cardFilterArray;

	@FragmentArg(StaticData.ExtraKeys.SELECTED_PROVINCE_ID)
	static int provinceID;

	@FragmentArg(StaticData.ExtraKeys.PLACE_TYPE)
	PlaceType placeType;

	private boolean isRefreshOnAttach = false;

	private View layout;

	private ArrayList<FacePromo> promoDetailList;
	private EndlessPromoListAdapter mAdapter;

	public static ListPageFragment newInstance(PlaceType placeType,
			ArrayList<Integer> cardFilterArray, int provinceID) {
		ListPageFragment lpf = new ListPageFragment_();
		Bundle args = new Bundle();
		args.putSerializable(StaticData.ExtraKeys.PLACE_TYPE, placeType);
		args.putSerializable(StaticData.ExtraKeys.CARD_FILTER_ARRAY,
				cardFilterArray);
		args.putSerializable(StaticData.ExtraKeys.SELECTED_PROVINCE_ID,
				provinceID);
		lpf.setArguments(args);
		return lpf;
	}

	/** Called when the view is first created. */
	// @Override
	// public View onCreateView(LayoutInflater inflater, ViewGroup container,
	// Bundle savedInstanceState) {
	// super.onCreate(savedInstanceState);
	// layout = inflater.inflate(R.layout.fragment_list_page, null);
	//
	// return layout;
	// }

	@Override
	public void onActivityCreated(Bundle savedState) {
		super.onActivityCreated(savedState);

		promoDetailList = promoListDataSourceMap.get(placeType.getID());
		if (mAdapter == null) {
			if (promoDetailList == null) {
				// promoDetailList = DAOArrayListWrapper.getInstance(
				// this.getActivity()).getFacePromosBy(placeType.getID(),
				// cardFilterArray, GlobalData.getSelectedProvinceID(),
				// getOrderString(), AppConfigParams.LISTVIEW_LIMIT_COUNT,
				// 0);
				promoDetailList = DAOArrayListWrapper.getInstance(
						this.getActivity()).getGroupFacePromos(
						placeType.getID(), cardFilterArray,
						GlobalData.getSelectedProvinceID(), getOrderString(),
						AppConfigParams.LISTVIEW_LIMIT_COUNT, 0);
				promoListDataSourceMap.put(placeType.getID(), promoDetailList);
			}
			mAdapter = new EndlessPromoListAdapter(this.getActivity(),
					new PromoListAdapter(this.getActivity(),
							R.string.title_activity_main, promoDetailList,
							cardFilterArray), getPlaceTypeID(), cardFilterArray);

		}
		setListAdapter(mAdapter);

		if (isRefreshOnAttach) {
			// refresh(DAOArrayListWrapper.getInstance(this.getActivity())
			// .getFacePromosBy(this.getPlaceTypeID(),
			// this.getCardFilterArray(),
			// GlobalData.getSelectedProvinceID(),
			// ListPageFragment.getOrderString(),
			// AppConfigParams.LISTVIEW_LIMIT_COUNT, 0));
			refresh(DAOArrayListWrapper.getInstance(this.getActivity())
					.getGroupFacePromos(this.getPlaceTypeID(), cardFilterArray,
							GlobalData.getSelectedProvinceID(),
							getOrderString(),
							AppConfigParams.LISTVIEW_LIMIT_COUNT, 0));
			isRefreshOnAttach = false;
		}

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		// this.placeType = (PlaceType) this.getArguments().get(
		// StaticData.ExtraKeys.PLACE_TYPE);
		// ListPageFragment.cardFilterArray = (ArrayList<Integer>) this
		// .getArguments().getIntegerArrayList(
		// StaticData.ExtraKeys.CARD_FILTER_ARRAY);
		// ListPageFragment.setProvinceID(this.getArguments().getInt(
		// StaticData.ExtraKeys.SELECTED_PROVINCE_ID));

		// if (isRefreshOnAttach) {
		// // refresh(DAOArrayListWrapper.getInstance(this.getActivity())
		// // .getFacePromosBy(this.getPlaceTypeID(),
		// // this.getCardFilterArray(),
		// // GlobalData.getSelectedProvinceID(),
		// // ListPageFragment.getOrderString(),
		// // AppConfigParams.LISTVIEW_LIMIT_COUNT, 0));
		// refresh(DAOArrayListWrapper.getInstance(this.getActivity())
		// .getGroupFacePromos(this.getPlaceTypeID(), cardFilterArray,
		// GlobalData.getSelectedProvinceID(),
		// getOrderString(),
		// AppConfigParams.LISTVIEW_LIMIT_COUNT, 0));
		// isRefreshOnAttach = false;
		// }
	}

	@Override
	public void onListItemClick(ListView l, View v, int pos, long id) {
		Intent promoDetailIntent = new Intent(this.getActivity(),
				PromoDetailActivity_.class);
		promoDetailIntent.putExtra("selectedPromo",
				(FacePromo) mAdapter.getItem(pos));
		startActivity(promoDetailIntent);
	}

	public ArrayList<FacePromo> dataCheckAndReload() {
		if (isNeedToChangeData()) {
			return DAOArrayListWrapper.getInstance(this.getActivity())
					.getGroupFacePromos(getPlaceTypeID(), getCardFilterArray(),
							GlobalData.getSelectedProvinceID(),
							ListPageFragment.getOrderString(),
							AppConfigParams.LISTVIEW_LIMIT_COUNT, 0);
		}
		return null;
	}

	public void sortListPromo(ArrayList<FacePromo> promos) {
		if (promos != null) {
			this.promoDetailList = promos;

			promoListDataSourceMap.put(placeType.getID(), promoDetailList);
			mAdapter = new EndlessPromoListAdapter(this.getActivity(),
					new PromoListAdapter(this.getActivity(),
							R.string.title_activity_main, promoDetailList,
							cardFilterArray), getPlaceTypeID(), cardFilterArray);

			mAdapter.getWrappedAdapter().setCurAdapterSortFilterType(
					ListPageFragment.sortFilterType);
			mAdapter.getWrappedAdapter().setCurProvinceID(
					ListPageFragment.provinceID);
			this.setListAdapter(mAdapter);
		}
	}

	ProgressDialog progressDialog;

	@UiThread
	protected void showWaitDialog() {
		progressDialog = new ProgressDialog(this.getActivity());
		this.progressDialog.setMessage(this.getResources().getText(
				R.string.lbl_please_wait));
		this.progressDialog.show();
	}

	@UiThread
	protected void dismissWaitDialog() {
		this.progressDialog.dismiss();
	}

	@Background
	public void reloadData() {

		if (isNeedToChangeData()) {
			showWaitDialog();
			ArrayList<FacePromo> promos = DAOArrayListWrapper.getInstance(
					this.getActivity()).getGroupFacePromos(getPlaceTypeID(),
					getCardFilterArray(), GlobalData.getSelectedProvinceID(),
					ListPageFragment.getOrderString(),
					AppConfigParams.LISTVIEW_LIMIT_COUNT, 0);
			listRefresh(promos);
			dismissWaitDialog();
		}

	}

	@UiThread
	public void listRefresh(ArrayList<FacePromo> promos) {
		this.promoDetailList = promos;

		promoListDataSourceMap.put(placeType.getID(), promoDetailList);
		mAdapter = new EndlessPromoListAdapter(this.getActivity(),
				new PromoListAdapter(this.getActivity(),
						R.string.title_activity_main, promoDetailList,
						cardFilterArray), getPlaceTypeID(), cardFilterArray);

		mAdapter.getWrappedAdapter().setCurAdapterSortFilterType(
				ListPageFragment.sortFilterType);
		mAdapter.getWrappedAdapter().setCurProvinceID(
				ListPageFragment.provinceID);
		this.setListAdapter(mAdapter);
	}

	public boolean isNeedToChangeData() {
		promoDetailList = promoListDataSourceMap.get(placeType.getID());
		boolean isCardFilterChanged = !CardPromoUtils.arrayListToString(
				ListPageFragment.cardFilterArray).equalsIgnoreCase(
				CardPromoUtils.arrayListToString(mAdapter.getWrappedAdapter()
						.getCurCardFilterArray()));

		boolean isSortFilterTypeChanged = ListPageFragment.getSortFilterType() != mAdapter
				.getWrappedAdapter().getCurAdapterSortFilterType();

		boolean isProvinceIdChanged = ListPageFragment.getProvinceID() != mAdapter
				.getWrappedAdapter().getCurProvinceID();

		return promoDetailList != null
				&& mAdapter != null
				&& (isProvinceIdChanged || isSortFilterTypeChanged || isCardFilterChanged);
	}

	public void refresh(ArrayList<FacePromo> promoDetailList) {
		this.promoDetailList = promoDetailList;
		promoListDataSourceMap.put(placeType.getID(), promoDetailList);
		mAdapter = new EndlessPromoListAdapter(this.getActivity(),
				new PromoListAdapter(this.getActivity(),
						R.string.title_activity_main, promoDetailList,
						cardFilterArray), getPlaceTypeID(), cardFilterArray);

		mAdapter.getWrappedAdapter().setCurAdapterSortFilterType(
				ListPageFragment.sortFilterType);
		mAdapter.getWrappedAdapter().setCurProvinceID(
				ListPageFragment.provinceID);
		this.setListAdapter(mAdapter);
	}

	public static SortFilterType getSortFilterType() {
		return sortFilterType;
	}

	public static void setSortFilterType(SortFilterType sortFilterType) {
		ListPageFragment.sortFilterType = sortFilterType;

	}

	public static void setCardFilterArray(ArrayList<Integer> cardFilterArray) {
		ListPageFragment.cardFilterArray = cardFilterArray;
	}

	public int getPlaceTypeID() {
		return placeType.getID();
	}

	public static String getOrderString() {
		FacePromo fp = new FacePromo();
		switch (sortFilterType) {
		case BEST:
			return fp.clDiscount.name + StaticData.Db.ORDER_DESC;
		case NEWEST:
			return fp.clID.name + StaticData.Db.ORDER_DESC;
		case ALMOST_DONE:
			return fp.clExpireDate.name + StaticData.Db.ORDER_ASC;
		default:
			return null;
		}
	}

	public ArrayList<Integer> getCardFilterArray() {
		return cardFilterArray;
	}

	public boolean isRefreshOnAttach() {
		return isRefreshOnAttach;
	}

	public void setRefreshOnAttach(boolean isRefreshOnAttach) {
		this.isRefreshOnAttach = isRefreshOnAttach;
	}

	public ArrayList<FacePromo> getPromoDetailList() {
		return promoDetailList;
	}

	public void setPromoDetailList(ArrayList<FacePromo> promoDetailList) {
		this.promoDetailList = promoDetailList;
	}

	public static int getProvinceID() {
		return provinceID;
	}

	public static void setProvinceID(int provinceID) {
		ListPageFragment.provinceID = provinceID;
	}

}
