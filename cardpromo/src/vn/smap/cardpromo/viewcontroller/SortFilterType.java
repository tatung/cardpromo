package vn.smap.cardpromo.viewcontroller;

public enum SortFilterType {
	NEWEST,
	BEST,
	TOP_LIKE,
	ALMOST_DONE
}
