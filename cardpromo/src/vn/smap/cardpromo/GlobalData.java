package vn.smap.cardpromo;

import java.util.ArrayList;

import vn.smap.cardpromo.configs.AppConfigParams;
import vn.smap.cardpromo.utils.JsonUtils;
import vn.smap.cardpromo.utils.StaticData;
import vn.smap.dbprototype.dao.pojos.Province;
import android.content.SharedPreferences;
import android.widget.ArrayAdapter;

public class GlobalData {
	private static SharedPreferences settings = null;
	public static ArrayList<Integer> userCardsIDList = null;
	private static ArrayAdapter<Province> provinceArrayAdapter = null;

	private static int selectedProvincePos;

	public static void init(SharedPreferences settings) {
		GlobalData.settings = settings;
		userCardsIDList = JsonUtils.readJsonIDs(GlobalData.settings.getString(
				AppConfigParams.USER_CARDS_ID_JSON_STRING, "[]"));
		selectedProvincePos = settings.getInt(
				StaticData.PrefKeys.SELECTED_PROVINCE_POS, 0);
	}

	public static void deleteUserCard(int cardID) {
		while (GlobalData.userCardsIDList.remove((Integer) cardID))
			;
		GlobalData
				.getSettings()
				.edit()
				.putString(AppConfigParams.USER_CARDS_ID_JSON_STRING,
						JsonUtils.writeJsonIDs(GlobalData.userCardsIDList))
				.commit();
	}

	public static SharedPreferences getSettings() {
		return settings;
	}

	public static void setSettings(SharedPreferences settings) {
		GlobalData.settings = settings;
	}

	public static ArrayAdapter<Province> getProvinceArrayAdapter() {
		return provinceArrayAdapter;
	}

	public static void setProvinceArrayAdapter(
			ArrayAdapter<Province> provinceArrayAdapter) {
		GlobalData.provinceArrayAdapter = provinceArrayAdapter;
	}

	public static int getSelectedProvincePos() {
		return selectedProvincePos;
	}

	public static void setSelectedProvincePos(int selectedProvincePos) {
		GlobalData.selectedProvincePos = selectedProvincePos;
		GlobalData
				.getSettings()
				.edit()
				.putInt(StaticData.PrefKeys.SELECTED_PROVINCE_POS,
						selectedProvincePos).commit();
	}

	public static int getSelectedProvinceID() {
		return GlobalData.getProvinceArrayAdapter()
				.getItem(GlobalData.getSelectedProvincePos()).getID();
	}

}
