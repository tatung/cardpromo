package vn.smap.cardpromo.model;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import vn.smap.cardpromo.utils.StaticData;
import vn.smap.dbprototype.dao.dbworkers.BanksUtil;
import vn.smap.dbprototype.dao.dbworkers.CardsUtil;
import vn.smap.dbprototype.dao.dbworkers.DataBaseHelper;
import vn.smap.dbprototype.dao.dbworkers.FacePromosUtil;
import vn.smap.dbprototype.dao.dbworkers.PlaceTypesUtil;
import vn.smap.dbprototype.dao.dbworkers.ProvincesUtil;
import vn.smap.dbprototype.dao.pojos.Bank;
import vn.smap.dbprototype.dao.pojos.Card;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import vn.smap.dbprototype.dao.pojos.PlaceType;
import vn.smap.dbprototype.dao.pojos.Province;
import android.content.Context;
import android.database.SQLException;
import android.util.SparseArray;

public class DAOArrayListWrapper implements DAOWrapper {
	private static DAOArrayListWrapper mInstance = null;
	private DataBaseHelper myDbHelper = null;
	private static String currentDate; // yyyy-MM-dd

	public static DAOArrayListWrapper getInstance(Context ctx) {
		if (mInstance == null) {
			mInstance = new DAOArrayListWrapper(ctx.getApplicationContext());
			Calendar c = Calendar.getInstance();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			currentDate = df.format(c.getTime());
		}

		return mInstance;
	}

	private DAOArrayListWrapper(Context ctx) {
		myDbHelper = DataBaseHelper.getInstance(ctx);

		try {

			myDbHelper.createDataBase();

		} catch (IOException ioe) {

			throw new Error("Unable to create database");

		}

		try {

			myDbHelper.openDataBase();

		} catch (SQLException sqle) {

			throw sqle;

		}
	}

	/**
	 * get Banks APIs
	 * */
	@Override
	public ArrayList<Bank> getAllBanks() {
		Bank b = new Bank();
		BanksUtil bu = new BanksUtil(myDbHelper.getDb());
		ArrayList<Bank> bankList = bu.select(null, b.clShortName.name
				+ StaticData.Db.ORDER_ASC);

		return bankList;
	}

	/**
	 * get Cards APIs
	 * */
	@Override
	public ArrayList<Card> getAllBankCards() {
		ArrayList<Card> cardList = new CardsUtil(myDbHelper.getDb())
				.select(null);
		return cardList;
	}

	@Override
	public ArrayList<Card> getCardsByBankID(int bankID) {
		Card c = new Card();
		ArrayList<Card> cardList = new CardsUtil(myDbHelper.getDb())
				.select(c.clBankID.name + " = " + bankID);
		return cardList;
	}

	@Override
	public ArrayList<Card> getCardsByCardIDs(ArrayList<Integer> cardIDs) {

		Card c = new Card();
		String whereClause = "";
		ArrayList<Card> cardList = null;
		if (cardIDs != null && cardIDs.size() > 0) {
			whereClause += c.clID.name + " IN (";

			for (int i = 0; i < cardIDs.size(); i++) {
				if (i < cardIDs.size() - 1) {
					whereClause += cardIDs.get(i) + ", ";
				} else {
					whereClause += cardIDs.get(i) + ")";
				}
			}
			cardList = new CardsUtil(myDbHelper.getDb()).select(whereClause);
		} else {
			cardList = new ArrayList<Card>();
		}

		return cardList;
	}

	/**
	 * get PlaceTypes APIs
	 * */
	@Override
	public ArrayList<PlaceType> getAllPlaceTypes() {
		PlaceType pt = new PlaceType();
		ArrayList<PlaceType> placeTypeList = new PlaceTypesUtil(
				myDbHelper.getDb()).selectOrder(null, pt.clSort.name
				+ StaticData.Db.ORDER_ASC);
		return placeTypeList;
	}

	@Override
	public String getPlaceTypeName(int placeTypeID) {
		PlaceType pt = new PlaceType();
		String whereClause = pt.clID.name + " = " + placeTypeID;

		ArrayList<PlaceType> placeTypeList = new PlaceTypesUtil(
				myDbHelper.getDb()).select(whereClause);
		if (placeTypeList != null) {
			return placeTypeList.get(0).getName();
		}
		return null;
	}

	@Override
	public SparseArray<String> getPlaceTypeNameMap() {
		SparseArray<String> res = new SparseArray<String>();
		ArrayList<PlaceType> placeTypeList = new PlaceTypesUtil(
				myDbHelper.getDb()).select(null);
		for (PlaceType pt : placeTypeList) {
			res.put(pt.getID(), pt.getName());
		}
		return res;
	}

	/**
	 * get FacePromos APIs
	 * */
	@Override
	public ArrayList<FacePromo> getFacePromosByPlaceType(int placeTypeID) {
		FacePromo fp = new FacePromo();
		return new FacePromosUtil(myDbHelper.getDb())
				.select(fp.clPlaceTypeID.name + " = " + placeTypeID);
	}

	/*
	 * Return whereClause which filter by PlaceTypeID and ProvinceID and CardID
	 */
	private String buildFacePromoWhereClause(Integer placeTypeID,
			ArrayList<Integer> cardIDs, Integer provinceID) {
		FacePromo fp = new FacePromo();

		String whereClause = fp.clExpireDate.name + " >= '" + currentDate + "'";

		if (placeTypeID != null) {
			whereClause += " AND " + fp.clPlaceTypeID.name + " = "
					+ placeTypeID;
		}

		if (provinceID != null && provinceID != 0) {
			if (whereClause.length() > 0) {
				whereClause += " AND ";
			}
			whereClause += fp.clProvinceID.name + " = " + provinceID;
		}

		if (cardIDs != null && cardIDs.size() > 0) {
			if (whereClause.length() > 0) {
				whereClause += " AND ";
			}
			whereClause += fp.clCardID.name + " IN (";

			for (int i = 0; i < cardIDs.size(); i++) {
				if (i < cardIDs.size() - 1) {
					whereClause += cardIDs.get(i) + ", ";
				} else {
					whereClause += cardIDs.get(i) + ")";
				}
			}

		}
		return whereClause;
	}

	/*
	 * Return whereClause which filter by PlaceTypeID, CardID
	 */
	private String buildFacePromoWhereClause(int placeTypeID,
			ArrayList<Integer> cardIDs) {
		FacePromo fp = new FacePromo();
		String whereClause = fp.clPlaceTypeID.name + " = " + placeTypeID;

		if (cardIDs != null && cardIDs.size() > 0) {
			whereClause += " AND " + fp.clCardID.name + " IN (";

			for (int i = 0; i < cardIDs.size(); i++) {
				if (i < cardIDs.size() - 1) {
					whereClause += cardIDs.get(i) + ", ";
				} else {
					whereClause += cardIDs.get(i) + ")";
				}
			}

		}
		return whereClause;
	}

	/*
	 * Return whereClause which filter by PlaceTypeID and ProvinceID (all
	 * CardID)
	 */
	private String buildFacePromoWhereClause(int placeTypeID, int provinceID) {
		FacePromo fp = new FacePromo();
		String whereClause = fp.clPlaceTypeID.name + " = " + placeTypeID
				+ " AND " + fp.clProvinceID.name + " = " + provinceID;
		return whereClause;
	}

	/*
	 * Return FacePromo belongs to placeTypeID, have cardID is in cardIDs. If
	 * cardIDs == null --> return array with no item
	 */
	@Override
	public ArrayList<FacePromo> getFacePromosBy(int placeTypeID,
			ArrayList<Integer> cardIDs) {
		ArrayList<FacePromo> res = null;
		if (cardIDs == null || cardIDs.size() == 0) {
			res = new ArrayList<FacePromo>();
		} else {
			res = new FacePromosUtil(myDbHelper.getDb())
					.select(buildFacePromoWhereClause(placeTypeID, cardIDs));
		}
		return res;
	}

	@Override
	public ArrayList<FacePromo> getFacePromosBy(Integer placeTypeID,
			ArrayList<Integer> cardIDs, Integer provinceID, String order,
			Short limitCount, Integer offset) {
		ArrayList<FacePromo> res = null;
		if (cardIDs == null || cardIDs.size() == 0) {
			res = new ArrayList<FacePromo>();
		} else {
			res = new FacePromosUtil(myDbHelper.getDb())
					.select(buildFacePromoWhereClause(placeTypeID, cardIDs,
							provinceID), order, limitCount, offset);
		}
		return res;
	}

	public ArrayList<FacePromo> getGroupFacePromos(Integer placeTypeID,
			ArrayList<Integer> cardIDs, Integer provinceID, String order,
			Short limitCount, Integer offset) {
		FacePromo fp = new FacePromo();
		String groupBy = fp.clPlaceID.name + ", " + fp.clDiscount.name + ", "
				+ fp.clBankName.name;
		return new FacePromosUtil(myDbHelper.getDb()).select(null,
				buildFacePromoWhereClause(placeTypeID, cardIDs, provinceID),
				null, groupBy, order, limitCount, offset);

	}

	@Override
	public ArrayList<FacePromo> queryPromosBy(String keywords) {
		FacePromo fp = new FacePromo();
		String whereClause = fp.clPlaceName.name + " LIKE '%" + keywords
				+ "%' OR " + fp.clPlaceAddress.name + " LIKE '%" + keywords
				+ "%' OR " + fp.clConstrain.name + " LIKE '%" + keywords + "%'";
		return new FacePromosUtil(myDbHelper.getDb()).select(whereClause);
	}

	@Override
	public ArrayList<FacePromo> queryPromosBy(String keywords,
			Integer placeTypeID, ArrayList<Integer> cardIDs,
			Integer provinceID, String order, Short limitCount, Integer offset) {

		FacePromo fp = new FacePromo();

		String whereClause = buildFacePromoWhereClause(placeTypeID, cardIDs,
				provinceID);
		whereClause += " AND (" + fp.clPlaceName.name + " LIKE '%" + keywords
				+ "%' OR " + fp.clPlaceAddress.name + " LIKE '%" + keywords
				+ "%' OR " + fp.clConstrain.name + " LIKE '%" + keywords
				+ "%')";

		String groupBy = fp.clPlaceID.name + ", " + fp.clDiscount.name + ", "
				+ fp.clBankName.name;

		return new FacePromosUtil(myDbHelper.getDb()).select(null, whereClause,
				null, groupBy, order, limitCount, offset);

	}

	public ArrayList<FacePromo> TgetPromoBy(String whereClause) {
		ArrayList<FacePromo> res = new FacePromosUtil(myDbHelper.getDb())
				.select(whereClause);
		return res;
	}

	@Override
	public ArrayList<FacePromo> queryPromosBy(int placeTypeID,
			ArrayList<Integer> cardIDs, String keywords) {

		FacePromo fp = new FacePromo();

		String whereClause = fp.clPlaceTypeID.name + " = " + placeTypeID;

		if (cardIDs != null && cardIDs.size() > 0 && keywords != null) {
			whereClause += " AND " + fp.clCardID.name + " IN (";

			for (int i = 0; i < cardIDs.size(); i++) {
				if (i < cardIDs.size() - 1) {
					whereClause += cardIDs.get(i) + ", ";
				} else {
					whereClause += cardIDs.get(i) + ")";
				}
			}

			whereClause += " AND (" + fp.clPlaceName.name + " LIKE '%"
					+ keywords + "%' OR " + fp.clPlaceAddress.name + " LIKE '%"
					+ keywords + "%' OR " + fp.clPlaceAddress.name + " LIKE '%"
					+ keywords + "%')";
			return new FacePromosUtil(myDbHelper.getDb()).select(whereClause);
		}

		return null;
	}

	public ArrayList<Province> getAllProvinces() {
		ProvincesUtil bu = new ProvincesUtil(myDbHelper.getDb());
		ArrayList<Province> provinceList = bu.select(null);

		return provinceList;
	}

}
