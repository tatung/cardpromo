package vn.smap.cardpromo.model;

import java.util.ArrayList;

import vn.smap.cardpromo.utils.StaticData;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import android.content.Context;

public class CardPromoSearchEngine {
	private static CardPromoSearchEngine mInstance;
	private static DAOWrapper dbWrapper;
	private Context mCtx;

	public static CardPromoSearchEngine getInstance(Context ctx) {
		if (mInstance == null) {
			mInstance = new CardPromoSearchEngine(ctx.getApplicationContext());
		}
		return mInstance;
	}

	private CardPromoSearchEngine(Context ctx) {
		mCtx = ctx;
		dbWrapper = DAOArrayListWrapper.getInstance(mCtx);
	}

	public ArrayList<FacePromo> searchPromo(String keyword, Integer placeTypeID, ArrayList<Integer> cardIDs, Integer provinceID) {
		FacePromo fp = new FacePromo();
		return dbWrapper.queryPromosBy(keyword, placeTypeID, cardIDs, provinceID, fp.clDiscount.name + StaticData.Db.ORDER_DESC, null, null);
	}

}
