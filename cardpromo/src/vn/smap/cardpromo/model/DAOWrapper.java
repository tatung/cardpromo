package vn.smap.cardpromo.model;

import java.util.ArrayList;

import vn.smap.dbprototype.dao.pojos.Bank;
import vn.smap.dbprototype.dao.pojos.Card;
import vn.smap.dbprototype.dao.pojos.FacePromo;
import vn.smap.dbprototype.dao.pojos.PlaceType;
import android.util.SparseArray;

public interface DAOWrapper {
	public ArrayList<Bank> getAllBanks();

	public ArrayList<Card> getAllBankCards();

	public ArrayList<Card> getCardsByBankID(int bankID);

	public ArrayList<PlaceType> getAllPlaceTypes();

	public ArrayList<FacePromo> getFacePromosByPlaceType(int placeTypeID);

	public ArrayList<FacePromo> getFacePromosBy(int placeTypeID,
			ArrayList<Integer> cardIDs);

	public ArrayList<Card> getCardsByCardIDs(ArrayList<Integer> cardIDs);

	public ArrayList<FacePromo> queryPromosBy(String keywords);

	public ArrayList<FacePromo> queryPromosBy(int placeTypeID,
			ArrayList<Integer> cardIDs, String keywords);

	public String getPlaceTypeName(int placeTypeID);

	public SparseArray<String> getPlaceTypeNameMap();

	public ArrayList<FacePromo> getFacePromosBy(Integer placeTypeID,
			ArrayList<Integer> cardIDs, Integer provinceID, String order,
			Short limitCount, Integer offset);

	ArrayList<FacePromo> queryPromosBy(String keywords, Integer placeTypeID,
			ArrayList<Integer> cardIDs, Integer provinceID, String order,
			Short limitCount, Integer offset);

}
