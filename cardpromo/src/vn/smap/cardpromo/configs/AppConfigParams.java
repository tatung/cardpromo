package vn.smap.cardpromo.configs;


public class AppConfigParams {
	public static final String HOME_APP_DIR_NAME = "CardPromo";
	public static final String APP_DATA_DIR_NAME = "Data";
	public static final String APP_ASSETS_DIR_NAME = "Assets";
	public static final String APP_ASSETS_IMAGE_DIR_NAME = "Images";
	public static final String DATETIME_FORMAT="hh:mm dd-MM-yyyy";

	public static final String PREFS_NAME = "CardPromoPreferences";
	public static final String IS_FIRST_LAUNCH = "IsFirstLaunch";
	
	public static final String PROVINCE = "Province"; 

	public static final String IMG_PHOTO_NOT_AVAILABLE = "photo_not_available.jpg";

	public static final String USER_CARDS_ID_JSON_STRING = "UserCardIdsJsonString";
	
	public static final String ROOT_NAMESPACE = "vn.smap.cardpromo";
	
	public static final Short LISTVIEW_LIMIT_COUNT = 50;

	public static final String TEST_CARD_JSON_STRING = "[{\"ID\":1},{\"ID\":4},{\"ID\":5},{\"ID\":27},{\"ID\":33}]";
}
